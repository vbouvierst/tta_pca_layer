import gc
import logging
import glob

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim

from robustbench.data import load_cifar10c, load_cifar10
from robustbench.model_zoo.enums import ThreatModel
from robustbench.utils import load_model
from robustbench.utils import clean_accuracy as accuracy

from tqdm import tqdm
import sys
import os

from conf import cfg, load_cfg_fom_args

logger = logging.getLogger(__name__)

import pdb
from copy import deepcopy
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import collections
import gc
from pca import pca, incremental_pca, svd
import sys
import os
import matplotlib.pyplot as plt


class pca_every_batch(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """

    def __init__(self, model, optimizer, steps=1, episodic=False):
        super(pca_every_batch, self).__init__()
        self.model = model

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d):
                m.train()

        self.optimizer = optimizer
        self.steps = steps
        assert steps > 0, "TTAwPCA requires >= 1 step(s) to forward and update"
        self.episodic = episodic

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state, self.optimizer_state = copy_model_and_optimizer(self.model, self.optimizer)

    def forward(self, x):
        if self.episodic:
            self.reset()

        for step in tqdm(range(self.steps), file=sys.stdout):
            if step == self.steps - 1:
                for n in self.model.gammalayers.keys():
                    m = self.model.gammalayers[n]
                    m.last_step = True
            outputs = forward_and_adapt(x, self.model, self.optimizer)

            if (step == self.steps - 1) and self.plot:
                for n in self.model.gammalayers.keys():
                    m = self.model.gammalayers[n]
                    plt.plot(m.Xbffltr)
                    plt.savefig(os.path.join('/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE/filter_study',
                                             f'testplot_{n}{self.corruption}_Xbffltr.png'))
                    plt.close('all')
                    plt.plot(m.Xaffltr)
                    plt.savefig(os.path.join('/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE/filter_study',
                                             f'testplot_{n}{self.corruption}_Xaffltr.png'))
                    self.plot = False

        # for m in self.modules():
        #     if isinstance(m, GammaLayer):
        #         pass
        #         S_gamma = m.S_gamma.diag()
        #         print('Gamma', m.gamma)
        #         print(S_gamma)

        return outputs

    def reset(self):
        if self.model_state is None or self.optimizer_state is None:
            raise Exception("cannot reset without saved model/optimizer state")
        for n, m in self.model.named_modules():
            if isinstance(m, GammaLayer):
                m.reset()
        load_model_and_optimizer(self.model, self.optimizer,
                                 self.model_state, self.optimizer_state)


def configure_model(model, cfg):
    """Configure model for use with filter_pca."""

    model = ModuledNet(model, cfg)
    model.eval()

    model.requires_grad_(False)

    model.gammalayers = {}
    for n, m in model.named_modules():
        if isinstance(m, GammaLayer):
            m.requires_grad_(True)
            model.gammalayers[n] = m
    return model


@torch.jit.script
def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


@torch.enable_grad()  # ensure grads in possible no grad context for testing
def forward_and_adapt(x, model, optimizer):
    """Forward and adapt model on batch of data.

    Measure entropy of the model prediction, take gradients, and update params.
    """
    # forward
    outputs = model(x)
    # adapt
    loss = softmax_entropy(outputs).mean(0)
    # print(loss)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    return outputs


class GammaLayer(nn.Module):
    def __init__(
            self,
            init_gamma_max=False,
            cfg=None
    ):

        super(GammaLayer, self).__init__()

        self.cfg = cfg
        if self.cfg['data_type'] == 'freq': raise NotImplementedError
        self.gamma_diag = self.cfg.DIAG
        self.last_step = False
        self.test_time_adaptation = False
        self.S_gamma = None
        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        self.momentum = 0.01

        self.gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        if self.cfg.MULTIPLICATOR: self.mult = nn.parameter.Parameter(torch.Tensor([0.]).cuda())
        self.init_gamma_max = init_gamma_max
        self.typePCA = self.cfg.TYPE_PCA
        if self.typePCA == 'incremental':
            self.PCA = incremental_pca.IncrementalPCA()
        else:
            self.PCA = pca.PCA()
        self.filter = self.cfg.FILTER

        if 'exp' in self.filter:
            self.fc_filter = self.exp_filter
        elif 'quo' in self.filter:
            self.fc_filter = self.quo_filter
        elif 'cheb' in self.filter:
            self.fc_filter = self.simili_cheb_filter
        else:
            raise NotImplementedError

    def exp_filter(self):
        return 1. / (1. + (self.gamma ** 2 - self.PCA.explained_variance_).exp())

    def Id_addmult(self):
        return 1 + (self.gamma * self.PCA.explained_variance_)

    def quo_filter(self):
        return self.PCA.explained_variance_ / (self.PCA.explained_variance_ + F.relu(self.gamma))

    def simili_cheb_filter(self):
        return 1. / torch.sqrt(1. + self.gamma / self.PCA.explained_variance_)

    def forward(self, X):  # Input (N, C, H, W) => (N, C*H*W)
        if self.test_time_adaptation:
            if self.PCA.V is None:
                self.compute_pca(X.reshape(X.size(0), -1))
                self.PCA.explained_variance_ = self.PCA.explained_variance_.cuda()
                self.PCA.mean_ = self.PCA.mean_.cuda()
                self.PCA.V = self.PCA.V.cuda()
                self.PCA.Vt = self.PCA.Vt.cuda()
                self.PCA.singular_values_ = self.PCA.singular_values_.cuda()

            if self.cfg.MULTIPLICATOR:
                assert self.mult.requires_grad
                self.modulator = F.threshold(self.mult, 1., 1.)
                self.modulator = self.modulator.diag()

            self.S_gamma = self.fc_filter()

            # Reshape and center
            self.S_gamma = self.S_gamma.diag()
            xsize = X.size()
            X = X.reshape(xsize[0], -1)
            X_mean = X.mean(0)
            X -= X_mean
            X += self.PCA.mean_

            # To PCA basis
            X = torch.matmul(X, self.PCA.Vt)

            # Filtering
            if self.last_step: self.Xbffltr = X.t().detach().cpu().numpy()
            X = torch.matmul(X, self.S_gamma)
            if self.cfg.MULTIPLICATOR: X = torch.matmul(X, self.modulator)
            if self.last_step: self.Xaffltr = X.t().detach().cpu().numpy()

            # To original basis
            X = torch.matmul(X, self.PCA.V)

            # Shift and reshape
            X -= self.PCA.mean_
            X += X_mean

            X = X.reshape(xsize)
        return X

    @torch.no_grad()
    def clamp(self):
        self.gamma.data = torch.clamp(self.gamma.data.abs(), min=self.gamma_clamp)

    def compute_pca(self, X):  # Ici PCA en fin d'epoch, mieux avec IPCA ?
        self.PCA.fit(A=X)
        self.reset_gamma()

    def reset_gamma(self):
        if self.gamma_diag:
            self.gamma.data = torch.full_like(self.PCA.singular_values_, self.gamma_clamp).cuda()
            if self.cfg.MULTIPLICATOR:
                self.mult.data = torch.full_like(self.PCA.singular_values_, 1.).cuda()
        else:
            self.gamma.data = torch.Tensor([self.gamma_clamp]).cuda()
            if self.cfg.MULTIPLICATOR:
                self.mult.data = torch.Tensor([1.]).cuda()

    def reset(self):
        self.S_gamma = None
        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        self.momentum = 0.01

        self.gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        if self.typePCA == 'incremental':
            self.PCA = incremental_pca.IncrementalPCA()
        else:
            self.PCA = pca.PCA()


def collect_params(model):
    """Collect the affine scale + shift parameters from batch norms.

    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.

    Note: other choices of parameterization are possible!
    """
    params = []
    names = []
    for nm, m in model.named_modules():
        if isinstance(m, GammaLayer):
            for np, p in m.named_parameters():
                params.append(p)
                names.append(f"{nm}.{np}")
    return params, names


def load_model_and_optimizer(model, optimizer, model_state, optimizer_state):
    """Restore the model and optimizer states from copies."""
    model.load_state_dict(model_state, strict=True)
    optimizer.load_state_dict(optimizer_state)


def copy_model_and_optimizer(model, optimizer):
    """Copy the model and optimizer states for resetting after adaptation."""
    model_state = deepcopy(model.state_dict())
    optimizer_state = deepcopy(optimizer.state_dict())
    return model_state, optimizer_state


class ModuledNet(nn.Module):
    def __init__(self, model, cfg):
        super(ModuledNet, self).__init__()
        if cfg.MODEL.conv1.ENABLED:
            self.conv1 = nn.Sequential(*[model.conv1, GammaLayer(cfg=cfg.MODEL.conv1)])
        else:
            self.conv1 = model.conv1
        if cfg.MODEL.block1.ENABLED:
            self.block1 = nn.Sequential(*[model.block1, GammaLayer(cfg=cfg.MODEL.block1)])
        else:
            self.block1 = model.block1
        if cfg.MODEL.block2.ENABLED:
            self.block2 = nn.Sequential(*[model.block2, GammaLayer(cfg=cfg.MODEL.block2)])
        else:
            self.block2 = model.block2
        if cfg.MODEL.block3.ENABLED:
            self.block3 = nn.Sequential(*[model.block3, GammaLayer(cfg=cfg.MODEL.block3)])
        else:
            self.block3 = model.block3
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc,
            model.nChannels
        )
        if cfg.MODEL.CONVLIST.ENABLED:
            L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.Conv2d)][
                1:]  # model.conv1 special
            for i in range(10):
                remove = '.' + str(i) + '.'
                replace = '[' + str(i) + '].'
                for subind, sub in enumerate(L):
                    if remove in sub: L[subind] = sub[:sub.find(remove)] + replace + sub[
                                                                                     sub.find(remove) + len(remove):]
            for i in cfg.MODEL.CONVLIST.LIST:
                glayer = GammaLayer(cfg=cfg.MODEL.CONVLIST)
                glayer.list_index = i
                exec(f"{L[i]} = nn.Sequential(*[{L[i]}, glayer])")

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn
        self.relu = relu
        self.fc = fc
        self.nChannels = nChannels

    def forward(self, x):
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out)


def evaluate(description):
    load_cfg_fom_args(description)
    # configure model
    base_model = load_model(cfg.MODEL.ARCH, cfg.CKPT_DIR, cfg.CORRUPTION.DATASET, ThreatModel.corruptions).cuda()
    logger.info("test-time adaptation with PCA")
    model = setup_pca_every_batch(base_model)
    logger.info(f"Number of parameters: {count_parameters(model)}")

    if 'no_corruption' in cfg.CORRUPTION.TYPE:
        if cfg.MODEL.ADAPTATION in ["TTAwPCA", "polar_filter_pca"]: model.plot = True
        cfg.CORRUPTION.TYPE.remove('no_corruption')
        model.corruption = 'no_corruption'
        x_test, y_test = load_cifar10(n_examples=cfg.CORRUPTION.NUM_EX, data_dir=cfg.DATA_DIR)
        x_test, y_test = x_test.cuda(), y_test.cuda()
        acc = accuracy(model, x_test, y_test, cfg.TEST.BATCH_SIZE)
        err = 1. - acc
        logger.info(f"error % [no_corruption]: {err:.2%}")

    # evaluate on each severity and type of corruption in turn
    for severity in cfg.CORRUPTION.SEVERITY:
        for corruption_type in cfg.CORRUPTION.TYPE:
            # reset adaptation for each combination of corruption x severity
            # note: for evaluation protocol, but not necessarily needed
            model.corruption = corruption_type + str(severity)
            if cfg.MODEL.ADAPTATION in ["TTAwPCA", "polar_filter_pca"]: model.plot = False
            try:
                model.reset()
                logger.info("resetting model")
            except:
                logger.warning("not resetting model")

            x_test, y_test = load_cifar10c(n_examples=cfg.CORRUPTION.NUM_EX,
                                           severity=severity, data_dir=cfg.DATA_DIR, shuffle=False,
                                           corruptions=[corruption_type])
            x_test, y_test = x_test.cuda(), y_test.cuda()
            acc = accuracy(model, x_test, y_test, cfg.TEST.BATCH_SIZE)
            err = 1. - acc
            logger.info(f"error % [{corruption_type}{severity}]: {err:.2%}")


def setup_pca_every_batch(model):
    """Set up TTAwPCA.

    Add PCA layer to the existing layers of the fully trained model,
    collect the parameters for PCA filtering by gradient optimization regarding the entropy,
    set up the optimizer, and then TTAwPCA the model.
    """
    model = configure_model(model=model, cfg=cfg)

    # Collect training data of the model

    transform = transforms.Compose(
        [
            transforms.ToTensor()
        ]
    )

    for n, m in model.named_modules():
        if isinstance(m, GammaLayer):
            m.test_time_adaptation = True
        if isinstance(m, nn.BatchNorm2d):
            m.train()
            logger.info(f"Batchnorm {n} set at train")

    params, param_names = collect_params(model)
    optimizer = setup_optimizer(params)

    filter_model = pca_every_batch(
        model,
        optimizer,
        steps=cfg.OPTIM.STEPS,
        episodic=cfg.MODEL.EPISODIC
    )

    logger.info(f"model for adaptation: {filter_model}")
    logger.info(f"params for adaptation: {param_names}")
    logger.info(f"optimizer for adaptation: {optimizer}")

    return filter_model

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

def setup_optimizer(params):
    """Set up optimizer for tent adaptation.

    Tent needs an optimizer for test-time entropy minimization.
    In principle, tent could make use of any gradient optimizer.
    In practice, we advise choosing Adam or SGD+momentum.
    For optimization settings, we advise to use the settings from the end of
    trainig, if known, or start with a low learning rate (like 0.001) if not.

    For best results, try tuning the learning rate and batch size.
    """
    if cfg.OPTIM.METHOD == 'Adam':
        return optim.Adam(params,
                    lr=cfg.OPTIM.LR,
                    betas=(cfg.OPTIM.BETA, 0.999),
                    weight_decay=cfg.OPTIM.WD)
    elif cfg.OPTIM.METHOD == 'SGD':
        return optim.SGD(params,
                   lr=cfg.OPTIM.LR,
                   momentum=cfg.OPTIM.MOMENTUM,
                   dampening=cfg.OPTIM.DAMPENING,
                   weight_decay=cfg.OPTIM.WD,
                   nesterov=cfg.OPTIM.NESTEROV)
    else:
        raise NotImplementedError

if __name__ == '__main__':
    evaluate('"CIFAR-10-C evaluation.')