# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

"""Configuration file (powered by YACS)."""

import argparse
import os
import sys
import logging
import random
import torch
import numpy as np
from datetime import datetime
from iopath.common.file_io import g_pathmgr
from yacs.config import CfgNode as CfgNode


# Global config object (example usage: from core.config import cfg)
_C = CfgNode()
cfg = _C


# ----------------------------- Model options ------------------------------- #
_C.MODEL = CfgNode()

# Check https://github.com/RobustBench/robustbench for available models
_C.MODEL.ARCH = 'Standard'

# Choice of (source, norm, tent)
# - source: baseline without adaptation
# - norm: test-time normalization
# - tent: test-time entropy minimization (ours)
_C.MODEL.ADAPTATION = 'source'

# By default tent is online, with updates persisting across batches.
# To make adaptation episodic, and reset the model for each batch, choose True.
_C.MODEL.EPISODIC = True

_C.MODEL.LOSS = 'entropy'

_C.MODEL.RENYI_ORDER = 2

_C.MODEL.DIV_LOSS = CfgNode()
_C.MODEL.DIV_LOSS.enabled = False
_C.MODEL.DIV_LOSS.target_distr = 'uniform'
_C.MODEL.DIV_LOSS.k = 0.9

# ----------------------------- PCA options ------------------------------- #
_C.MODEL.conv1 = CfgNode()
_C.MODEL.conv1.ENABLED = False
_C.MODEL.conv1.VARIANCE_NORM = False
_C.MODEL.conv1.data_type = 'spat'
_C.MODEL.conv1.DIAG = True
_C.MODEL.conv1.C = 1000.
_C.MODEL.conv1.VOLUME_PRESERVING = False
_C.MODEL.conv1.METHOD = None
_C.MODEL.conv1.TYPE_PCA = 'classic'
_C.MODEL.conv1.PCA_RANK = 1500
_C.MODEL.conv1.FORCE_PCA = False
_C.MODEL.conv1.FILTER = 'exp'
_C.MODEL.conv1.MULTIPLICATOR = False

_C.MODEL.block1 = CfgNode()
_C.MODEL.block1.ENABLED = False
_C.MODEL.block1.VARIANCE_NORM = False
_C.MODEL.block1.data_type = 'spat'
_C.MODEL.block1.DIAG = True
_C.MODEL.block1.C = 1000000.
_C.MODEL.block1.VOLUME_PRESERVING = False
_C.MODEL.block1.METHOD = None
_C.MODEL.block1.TYPE_PCA = 'classic'
_C.MODEL.block1.PCA_RANK = 1500
_C.MODEL.block1.FORCE_PCA = False
_C.MODEL.block1.FILTER = 'exp'
_C.MODEL.block1.MULTIPLICATOR = False

_C.MODEL.block2 = CfgNode()
_C.MODEL.block2.ENABLED = False
_C.MODEL.block2.VARIANCE_NORM = False
_C.MODEL.block2.data_type = 'spat'
_C.MODEL.block2.DIAG = True
_C.MODEL.block2.C = 1000000.
_C.MODEL.block2.VOLUME_PRESERVING = False
_C.MODEL.block2.METHOD = None
_C.MODEL.block2.TYPE_PCA = 'classic'
_C.MODEL.block2.PCA_RANK = 1500
_C.MODEL.block2.FORCE_PCA = False
_C.MODEL.block2.FILTER = 'exp'
_C.MODEL.block2.MULTIPLICATOR = False

_C.MODEL.block3 = CfgNode()
_C.MODEL.block3.ENABLED = False
_C.MODEL.block3.VARIANCE_NORM = False
_C.MODEL.block3.data_type = 'spat'
_C.MODEL.block3.DIAG = True
_C.MODEL.block3.C = 1000000.
_C.MODEL.block3.VOLUME_PRESERVING = False
_C.MODEL.block3.METHOD = None
_C.MODEL.block3.TYPE_PCA = 'classic'
_C.MODEL.block3.PCA_RANK = 1500
_C.MODEL.block3.FORCE_PCA = False
_C.MODEL.block3.FILTER = 'exp'
_C.MODEL.block3.MULTIPLICATOR = False

_C.MODEL.BN = CfgNode()
_C.MODEL.BN.RUNNING_STATS = True
_C.MODEL.BN.ENABLED = False
_C.MODEL.BN.VARIANCE_NORM = False
_C.MODEL.BN.data_type = 'spat'
_C.MODEL.BN.DIAG = True
_C.MODEL.BN.C = 1000000.
_C.MODEL.BN.VOLUME_PRESERVING = False
_C.MODEL.BN.METHOD = None
_C.MODEL.BN.TYPE_PCA = 'classic'
_C.MODEL.BN.PCA_RANK = 1500
_C.MODEL.BN.FORCE_PCA = False
_C.MODEL.BN.FILTER = 'exp'
_C.MODEL.BN.MULTIPLICATOR = False
_C.MODEL.BN.ONLYTRAIN = False

_C.MODEL.CONVLIST = CfgNode()
_C.MODEL.CONVLIST.ENABLED = False
_C.MODEL.CONVLIST.VARIANCE_NORM = False
_C.MODEL.CONVLIST.data_type = 'spat'
_C.MODEL.CONVLIST.DIAG = True
_C.MODEL.CONVLIST.C = 1000000.
_C.MODEL.CONVLIST.VOLUME_PRESERVING = False
_C.MODEL.CONVLIST.METHOD = None
_C.MODEL.CONVLIST.TYPE_PCA = 'incremental'
_C.MODEL.CONVLIST.PCA_RANK = 1500
_C.MODEL.CONVLIST.FORCE_PCA = False
_C.MODEL.CONVLIST.FILTER = 'exp'
_C.MODEL.CONVLIST.LIST = []
_C.MODEL.CONVLIST.MULTIPLICATOR = False

# ----------------------------- Corruption options -------------------------- #
_C.CORRUPTION = CfgNode()

# Dataset for evaluation
_C.CORRUPTION.DATASET = 'cifar10'

# Check https://github.com/hendrycks/robustness for corruption details
_C.CORRUPTION.TYPE = ['gaussian_noise', 'shot_noise', 'impulse_noise',
                      'defocus_blur', 'glass_blur', 'motion_blur', 'zoom_blur',
                      'snow', 'frost', 'fog', 'brightness', 'contrast',
                      'elastic_transform', 'pixelate', 'jpeg_compression']
_C.CORRUPTION.SEVERITY = [5]

# Number of examples to evaluate (10000 for all samples in CIFAR-10)
_C.CORRUPTION.NUM_EX = 10000

# ------------------------------- Batch norm options ------------------------ #
_C.BN = CfgNode()

# BN epsilon
_C.BN.EPS = 1e-5

# BN momentum (BN momentum in PyTorch = 1 - BN momentum in Caffe2)
_C.BN.MOM = 0.1

# ------------------------------- Optimizer options ------------------------- #
_C.OPTIM = CfgNode()

# Number of updates per batch
_C.OPTIM.STEPS = 1

# Learning rate
_C.OPTIM.LR = 1e-3

# Choices: Adam, SGD
_C.OPTIM.METHOD = 'Adam'

# Beta
_C.OPTIM.BETA = 0.9

# Momentum
_C.OPTIM.MOMENTUM = 0.9

# Momentum dampening
_C.OPTIM.DAMPENING = 0.0

# Nesterov momentum
_C.OPTIM.NESTEROV = True

# L2 regularization
_C.OPTIM.WD = 0.0

# ------------------------------- Testing options --------------------------- #
_C.TEST = CfgNode()

# Batch size for evaluation (and updates for norm + tent)
_C.TEST.BATCH_SIZE = 128

# --------------------------------- CUDNN options --------------------------- #
_C.CUDNN = CfgNode()

# Benchmark to select fastest CUDNN algorithms (best for fixed input sizes)
_C.CUDNN.BENCHMARK = True

# ---------------------------------- Misc options --------------------------- #

# Optional description of a config
_C.DESC = ""

# Note that non-determinism is still present due to non-deterministic GPU ops
_C.RNG_SEED = 1

# Output directory
_C.SAVE_DIR = "/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE"

# Data directory
_C.DATA_DIR = "/gpfs/workdir/cordiert/datasets/TTA_pca_data"

# Weight directory
_C.CKPT_DIR = "/gpfs/workdir/cordiert/checkpoints/tta_pca/CKPT"

# Log destination (in SAVE_DIR)
_C.LOG_DEST = "log.txt"

# Log datetime
_C.LOG_TIME = ''

# # Config destination (in SAVE_DIR)
# _C.CFG_DEST = "cfg.yaml"

# --------------------------------- Default config -------------------------- #
_CFG_DEFAULT = _C.clone()
_CFG_DEFAULT.freeze()


def assert_and_infer_cfg():
    """Checks config values invariants."""
    err_str = "Unknown adaptation method."
    assert _C.MODEL.ADAPTATION in ["source", "norm", "tent"]
    err_str = "Log destination '{}' not supported"
    assert _C.LOG_DEST in ["stdout", "file"], err_str.format(_C.LOG_DEST)


def merge_from_file(cfg_file):
    with g_pathmgr.open(cfg_file, "r") as f:
        cfg = _C.load_cfg(f)
    _C.merge_from_other_cfg(cfg)


def dump_cfg():
    """Dumps the config to the output directory."""
    cfg_file = os.path.join(_C.SAVE_DIR, _C.CFG_DEST)
    with g_pathmgr.open(cfg_file, "w") as f:
        _C.dump(stream=f)


def load_cfg(out_dir, cfg_dest="config.yaml"):
    """Loads config from specified output directory."""
    cfg_file = os.path.join(out_dir, cfg_dest)
    merge_from_file(cfg_file)


def reset_cfg():
    """Reset config to initial state."""
    cfg.merge_from_other_cfg(_CFG_DEFAULT)


def load_cfg_fom_args(description="Config options."):
    """Load config from command line args and set any specified options."""
    current_time = datetime.now().strftime("%y%m%d_%H%M%S")
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("--cfg", dest="cfg_file", type=str, required=True,
                        help="Config file location")
    parser.add_argument("opts", default=None, nargs=argparse.REMAINDER,
                        help="See conf.py for all options")
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()

    merge_from_file(args.cfg_file)
    cfg.merge_from_list(args.opts)

    log_dest = os.path.basename(args.cfg_file)
    log_dest = log_dest.replace('.yaml', '_{}.txt'.format(current_time))

    g_pathmgr.mkdirs(cfg.SAVE_DIR)
    cfg.LOG_TIME, cfg.LOG_DEST = current_time, log_dest
    cfg.freeze()

    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s] [%(filename)s: %(lineno)4d]: %(message)s",
        datefmt="%y/%m/%d %H:%M:%S",
        handlers=[
            logging.FileHandler(os.path.join(cfg.SAVE_DIR, cfg.LOG_DEST)),
            logging.StreamHandler(sys.stdout)
        ])

    np.random.seed(cfg.RNG_SEED)
    torch.manual_seed(cfg.RNG_SEED)
    random.seed(cfg.RNG_SEED)
    torch.backends.cudnn.benchmark = cfg.CUDNN.BENCHMARK

    logger = logging.getLogger(__name__)
    version = [torch.__version__, torch.version.cuda,
               torch.backends.cudnn.version()]
    logger.info(
        "PyTorch Version: torch={}, cuda={}, cudnn={}".format(*version))
    logger.info(cfg)
