import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import os

import pdb
from copy import deepcopy
from tqdm import tqdm
import sys
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from scipy.linalg import lu
import pandas as pd
import pylab as pl


class DisCorr(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """

    def __init__(self, model, optimizer, run, steps=1, episodic=False):
        super(DisCorr, self).__init__()
        self.model = model
        self.run = run

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d):
                m.train()

        # for m in model.modules():
        #     if isinstance(m, Corrlayer):
        #         m.no_batch = self.no_batch

        self.optimizer = optimizer
        self.steps = steps
        self.episodic = episodic

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state = copy_model(self.model)

    def forward(self, x):
        if self.episodic:
            self.reset()

        for _ in tqdm(range(self.steps), file=sys.stdout):
            outputs = forward_and_adapt(x, self.model)

        return outputs

    def reset(self):
        if self.model_state is None:
            raise Exception("cannot reset without saved model state")
        load_model(self.model, self.model_state)
        for nm, m in self.named_modules():
            if isinstance(m, Corrlayer):
                m.corruption = self.corruption


def configure_model(model, cfg):
    """Configure model for use with filter_pca."""

    model = ModuledNet(model, cfg)
    model.eval()

    model.requires_grad_(False)

    for m in model.modules():
        if isinstance(m, Corrlayer):
            m.requires_grad_(True)
    return model


def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


@torch.enable_grad()  # ensure grads in possible no grad context for testing
def forward_and_adapt(x, model):
    """Forward and adapt model on batch of data.

    Measure entropy of the model prediction, take gradients, and update params.
    """
    # forward
    outputs = model(x)
    # self.run.log({f"loss": loss})
    # # adapt
    # loss = softmax_entropy(outputs).mean(0)
    # # print(loss)
    # loss.backward()
    # optimizer.step()
    # optimizer.zero_grad()
    return outputs


class Corrlayer(nn.Module):
    def __init__(self, METHOD=None):

        super(Corrlayer, self).__init__()
        self.METHOD = METHOD
        self.register_flag = False
        self.test_time_adaptation = False
        self.activations = None
        self.no_batch = 0
        self.color = False

    def forward(self, X):  # Input (N, C, H, W) => (N, C*H*W)
        if self.register_flag:
            with torch.no_grad():
                self.activations = torch.cat(
                    (
                        self.activations.cpu(),
                        X.flatten(1, -1).cpu()
                    )
                ) if self.activations is not None else X.flatten(1, -1).cpu()
        elif self.test_time_adaptation:
            xsize = X.size()
            X2 = X.flatten(1, -1)
            X2 -= X2.mean(0)
            n_samples = X2.size(0)
            n_features = X2.size(1)
            if self.METHOD == 'PCA' or self.METHOD == 'EXP_VAR': X2 = torch.from_numpy(self.pca.transform(X2.cpu().numpy())).cuda()
            if self.METHOD == 'PCA' or self.METHOD == 'LU': X2 = torch.matmul(X2, self.invcholes)
            if self.METHOD == 'EXP_VAR': X2 = X2[:,self.pca.explained_variance_>1e-3] / torch.from_numpy(self.pca.explained_variance_[self.pca.explained_variance_>1e-3]).cuda().sqrt()

            X2_color = X2 / X2.std(0)
            X2_color = X2_color * self.train_std

            covariance = 1. / (xsize[0]-1) * torch.matmul(X2.t(), X2)
            covariance_color = 1. / (xsize[0]-1) * torch.matmul(X2_color.t(), X2_color)

            lbd_diff = torch.linalg.svd(covariance)[1]
            lbd_diff = lbd_diff[lbd_diff > 1e-3]

            lbd_diff_color = torch.linalg.svd(covariance_color)[1]
            lbd_diff_color = lbd_diff_color[lbd_diff_color > 1e-3]

            lbd = torch.Tensor([n_features / n_samples])
            self.lbd_min = (1 - torch.sqrt(lbd)) ** 2
            self.lbd_max = (1 + torch.sqrt(lbd)) ** 2

            # print(self.lbd_min, self.lbd_max)
            plot_lbd(corruption=self.corruption, module_name=self.name, no_batch=self.no_batch,
                     savefolder=self.savefolder, lbd_diff=lbd_diff, lbd_min=self.lbd_min, lbd_max=self.lbd_max,
                     METHOD=self.METHOD, color=False)
            plot_lbd(corruption=self.corruption, module_name=self.name, no_batch=self.no_batch,
                     savefolder=self.savefolder, lbd_diff=lbd_diff_color, lbd_min=self.lbd_min, lbd_max=self.lbd_max,
                     METHOD=self.METHOD, color=True)
            self.no_batch += 1
        return X


    def enable_register(self):
        self.register_flag = True
        print('Careful: register_flag is now enabled.')

    def prepare_disentangle(self):
        assert self.activations is not None
        self.mean_ = torch.mean(self.activations, dim=0)
        self.activations -= self.mean_
        if self.METHOD == 'PCA' or self.METHOD == 'EXP_VAR':
            self.pca = PCA(n_components=5000)
            self.activations = torch.from_numpy(self.pca.fit_transform(self.activations.numpy()))
        self.train_std = self.activations.std(0).cuda()

        if self.METHOD == 'LU':
            covariance = 1. / (self.activations.size(0) - 1) * torch.matmul(self.activations.t(), self.activations)
            _, _, cov_U = lu(covariance)
            self.invcholes = torch.linalg.inv(torch.from_numpy(cov_U)).cuda()
        elif self.METHOD == 'PCA':
            covariance = 1. / (self.activations.size(0) - 1) * torch.matmul(self.activations.t(), self.activations)
            self.invcholes = torch.linalg.inv(torch.linalg.cholesky(covariance).t()).cuda()
        # elif self.METHOD == 'EXP_VAR':
        #     self.activations /= torch.from_numpy(self.pca.explained_variance_).sqrt()
        #     covariance = 1. / (self.activations.size(0) - 1) * torch.matmul(self.activations.t(), self.activations)
        self.activations = None


def cov(X):
    D = X.shape[-1]
    mean = torch.mean(X, dim=-1).unsqueeze(-1)
    X = X - mean
    return 1/(D-1) * X @ X.transpose(-1, -2)


def corr(X, eps=1e-08):
    D = X.shape[-1]
    std = torch.std(X, dim=-1).unsqueeze(-1)
    mean = torch.mean(X, dim=-1).unsqueeze(-1)
    X = (X - mean) / (std + eps)
    return 1/(D-1) * X @ X.transpose(-1, -2)


def load_model(model, model_state):
    """Restore the model state from copies."""
    model.load_state_dict(model_state, strict=True)

def copy_model(model):
    """Copy the model state for resetting after adaptation."""
    model_state = deepcopy(model.state_dict())
    return model_state

def plot_lbd(corruption, module_name, no_batch, savefolder, lbd_diff, lbd_min, lbd_max, METHOD, color):
    color = 'color' if color else ''
    # fig = plt.figure(figsize=(8,8))
    # plt.title(f"SVD correlation matrices of {module_name} on {corruption} (n°{no_batch}) {LU}")
    # plt.hist(lbd_diff, bins=100, alpha=0.5)
    # plt.axvline(lbd_min)
    # plt.axvline(lbd_max)
    # fig.set_xlim((0, lbd_max*10))
    # fig.set_ylim((0, lbd_diff.max()))
    #
    # fig.savefig(os.path.join(savefolder, 'histograms', 'corr_'+str(no_batch)+'_'+module_name+'_'+corruption+'_'+LU+'test'+'.png'))
    # plt.close('all')
    df = pd.DataFrame(lbd_diff.cpu().numpy(), columns = ['lbd_diff'])
    ax = df.plot.hist(bins=100, alpha=0.5)
    fig = ax.get_figure()
    ymin, ymax = ax.get_ylim()
    ax.vlines(x=[lbd_min.cpu().numpy(), lbd_max.cpu().numpy()], ymin=ymin, ymax=ymax-1, color='r')
    pl.suptitle(f"SVD correlation matrices of {module_name} on {corruption} (n°{no_batch}) {METHOD}")
    fig.savefig(os.path.join(savefolder, 'histograms', 'corr_'+str(no_batch)+'_'+module_name+'_'+corruption+'_'+METHOD+color+'BN+NOinvcholes.png'))
    plt.close('all')


class ModuledNet(nn.Module):
    def __init__(self, model, cfg):
        super(ModuledNet, self).__init__()
        if cfg.MODEL.conv1.ENABLED:
            self.conv1 = nn.Sequential(*[model.conv1, Corrlayer(METHOD=cfg.MODEL.conv1.METHOD)])
        else:
            self.conv1 = model.conv1
        if cfg.MODEL.block1.ENABLED:
            self.block1 = nn.Sequential(*[model.block1, Corrlayer(METHOD=cfg.MODEL.block1.METHOD)])
        else:
            self.block1 = model.block1
        if cfg.MODEL.block2.ENABLED:
            self.block2 = nn.Sequential(*[model.block2, Corrlayer(METHOD=cfg.MODEL.block2.METHOD)])
        else:
            self.block2 = model.block2
        if cfg.MODEL.block3.ENABLED:
            self.block3 = nn.Sequential(*[model.block3, Corrlayer(METHOD=cfg.MODEL.block3.METHOD)])
        else:
            self.block3 = model.block3
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc,
            model.nChannels
        )

        if cfg.MODEL.BN.ENABLED:
            L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.BatchNorm2d)]
            LL = []
            for i in range(10):
                remove = '.' + str(i) + '.'
                replace = '[' + str(i) + '].'
                for sub in L:
                    if remove in sub:
                        sub = sub[:sub.find(remove)] + replace + sub[sub.find(remove) + len(remove):]
                        LL.append(sub)
            for i in range(len(LL)):
                exec(
                    f"{LL[i]} = nn.Sequential(*[{LL[i]}, Corrlayer(METHOD=cfg.MODEL.BN.METHOD)])")

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn
        self.relu = relu
        self.fc = fc
        self.nChannels = nChannels

    def forward(self, x):
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out)