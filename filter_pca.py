import pdb
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import collections
import gc
from pca import pca, incremental_pca, svd
import sys
import os
import matplotlib.pyplot as plt
import wandb

from utils import load_model_and_optimizer, copy_model_and_optimizer
from loss import softmax_entropy, hard_likelihood_ratio, soft_likelihood_ratio, DivLoss, Renyi_entropy


class TTAwPCA(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """
    def __init__(self, model, optimizer, cfg, run):
        super(TTAwPCA, self).__init__()
        self.model = model
        self.run = run

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d): 
                m.train()

        self.optimizer = optimizer
        self.steps = cfg.OPTIM.STEPS
        assert self.steps > 0, "TTAwPCA requires >= 1 step(s) to forward and update"
        self.episodic = cfg.MODEL.EPISODIC

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state, self.optimizer_state = copy_model_and_optimizer(self.model, self.optimizer)

        if cfg.MODEL.LOSS == 'entropy':
            self.loss_fn = softmax_entropy
        elif cfg.MODEL.LOSS == 'HLR':
            self.loss_fn = hard_likelihood_ratio
        elif cfg.MODEL.LOSS == 'SLR':
            self.loss_fn = soft_likelihood_ratio
        elif cfg.MODEL.LOSS == 'Renyi':
            self.loss_fn = Renyi_entropy(order=cfg.MODEL.RENYI_ORDER)
        else:
            raise NotImplementedError

        if cfg.MODEL.DIV_LOSS.enabled:
            self.add_loss = DivLoss(target_distr=cfg.MODEL.DIV_LOSS.target_distr, k=cfg.MODEL.DIV_LOSS.k)
        else:
            self.add_loss = None

    def forward(self, x):
        if self.episodic:
            self.reset()

        for step in tqdm(range(self.steps), file=sys.stdout):
            if step == self.steps-1:
                for n in self.model.gammalayers.keys():
                    m = self.model.gammalayers[n]
                    m.last_step = True
            outputs = self.forward_and_adapt(x)

            if (step == self.steps - 1) and self.plot:
                for n in self.model.gammalayers.keys():
                    m = self.model.gammalayers[n]
                    plt.plot(m.Xbffltr)
                    plt.savefig(os.path.join('/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE/filter_study', f'testplot_{n}{self.corruption}_Xbffltr.png'))
                    plt.close('all')
                    plt.plot(m.Xaffltr)
                    plt.savefig(os.path.join('/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE/filter_study', f'testplot_{n}{self.corruption}_Xaffltr.png'))
                    self.plot = False


        # for m in self.modules():
        #     if isinstance(m, GammaLayer):
        #         pass
        #         S_gamma = m.S_gamma.diag()
        #         print('Gamma', m.gamma)
        #         print(S_gamma)

        return outputs

    def reset(self):
        if self.model_state is None or self.optimizer_state is None:
            raise Exception("cannot reset without saved model/optimizer state")
        load_model_and_optimizer(self.model, self.optimizer,
                                 self.model_state, self.optimizer_state)

    @torch.enable_grad()  # ensure grads in possible no grad context for testing
    def forward_and_adapt(self, x):
        """Forward and adapt model on batch of data.

        Measure entropy of the model prediction, take gradients, and update params.
        """
        # forward
        outputs = self.model(x)
        # adapt
        loss = self.loss_fn(outputs).mean(0)
        self.run.log({f"{self.corruption}_loss": loss})
        if self.add_loss:
            loss += self.add_loss(outputs).mean(0)
            self.run.log({f"{self.corruption}_add_loss": self.add_loss(outputs).mean(0)})
        # print(loss)
        loss.backward()
        self.optimizer.step()
        self.optimizer.zero_grad()
        return outputs

def configure_model(model, cfg):
    """Configure model for use with filter_pca."""

    model = ModuledNet(model, cfg)
    model.eval()

    model.requires_grad_(False)

    model.gammalayers = {}
    for n, m in model.named_modules():
        if isinstance(m, GammaLayer):
            m.requires_grad_(True)
            model.gammalayers[n] = m
    return model


class GammaLayer(nn.Module):
    def __init__(
            self,
            init_gamma_max=False,
            cfg=None
        ):
        
        super(GammaLayer, self).__init__()

        self.cfg = cfg
        if self.cfg['data_type'] == 'freq': raise NotImplementedError
        self.gamma_diag = self.cfg.DIAG
        self.variance_norm = self.cfg.VARIANCE_NORM
        self.last_step = False
        self.register_flag = False
        self.test_time_adaptation = False
        self.activations = None
        self.covariance = None
        self.S_gamma = None
        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        self.momentum = 0.01
        self.C = self.cfg.C
        self.epsilon = 1e-6

        if self.cfg['data_type'] == 'freq':
            self.gamma = nn.parameter.Parameter(torch.view_as_complex(torch.Tensor([self.gamma_init]*2)).cuda())
        else:
            self.gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        if self.cfg.MULTIPLICATOR: self.mult = nn.parameter.Parameter(torch.Tensor([0.]).cuda())
        self.init_gamma_max = init_gamma_max
        self.typePCA = self.cfg.TYPE_PCA
        if self.typePCA == 'channel':
            self.PCA = svd.SVD()
        elif self.typePCA == 'incremental':
            self.PCA = incremental_pca.IncrementalPCA(rank=self.cfg.PCA_RANK)
        else:
            self.PCA = pca.PCA(rank=self.cfg.PCA_RANK)
        self.filter = self.cfg.FILTER

        if 'exp' in self.filter:
            self.fc_filter = self.exp_filter
        elif 'quo' in self.filter:
            self.fc_filter = self.quo_filter
        elif 'cheb' in self.filter:
            self.fc_filter = self.simili_cheb_filter
        elif 'quoC' in self.filter:
            self.fc_filter = self.quoC_filter
        else:
            raise NotImplementedError

    def init_pre_S_gamma(self):
        if self.S_gamma:
            pre_S_gamma = self.fc_filter()
        else:
            pre_S_gamma = self.S_gamma
        return pre_S_gamma

    def exp_filter(self):
        return 1. / (1. + (self.gamma ** 2 - self.PCA.explained_variance_).exp())

    def Id_addmult(self):
        return 1+(self.gamma*self.PCA.explained_variance_)

    def quo_filter(self):
        return self.PCA.explained_variance_ / (self.PCA.explained_variance_ + F.relu(self.gamma))

    def simili_cheb_filter(self):
        return 1. / torch.sqrt(1. + self.gamma / self.PCA.explained_variance_)

    def quoC_filter(self):
        return self.PCA.explained_variance_ / (self.C/torch.arange(1, self.PCA.explained_variance_.size(0)).cuda() * F.relu(self.gamma) + self.PCA.explained_variance_)

    def register_channel(self, X):
        # Permute and reshape
        Xp = X.permute((1, 2, 3, 0)).flatten(1, 2)
        N = Xp.shape[2]
        means = Xp.mean(dim=2, keepdim=True)
        m1 = Xp - means
        cov = torch.einsum('ijk,ilk->ijl', m1, m1) / (N - 1)
        if self.covariance is None:
            self.PCA.mean_ = means
            self.covariance = cov
        else:
            self.PCA.mean_ = (1. - self.momentum) * self.PCA.mean_ + self.momentum * means
            self.covariance = (1. - self.momentum) * self.covariance + self.momentum * cov

    def forward(self, X):  # Input (N, C, H, W) => (N, C*H*W)
        if self.register_flag:
            with torch.no_grad():
                if self.typePCA == 'channel':
                    self.register_channel(X)
                else:
                    self.activations = torch.cat(
                        (
                        self.activations.cpu(),
                        X.view(X.size(0), -1).cpu()
                        )
                        ) if self.activations is not None else X.view(X.size(0), -1).cpu()
        elif self.test_time_adaptation:
            assert hasattr(self.PCA, 'V'), "V is not defined. Make sure to self.compute_pca()"
            assert hasattr(self.PCA, 'Vt'), "Vt is not defined. Make sure to self.compute_pca()"
            assert self.gamma.requires_grad
            if self.cfg.MULTIPLICATOR:
                assert self.mult.requires_grad
                self.modulator = F.threshold(self.mult, 1., 1.)
                self.modulator = self.modulator.diag()

            # #self.clamp()
            if self.cfg.VOLUME_PRESERVING: self.pre_S_gamma = self.init_pre_S_gamma()
            self.S_gamma = self.fc_filter()
            if self.cfg.VOLUME_PRESERVING: self.S_gamma += 1/self.epsilon * (1 - (self.S_gamma.sum()/self.pre_S_gamma.sum().detach()))

            # Reshape and center
            if self.typePCA == 'channel':
                X = X.permute((1,0,2,3))
                xsize = X.size()
                X = X.flatten(2,3)
                X -= self.PCA.mean_.unsqueeze(1)
            else:
                self.S_gamma = self.S_gamma.diag()
                xsize = X.size()
                if self.cfg['data_type'] == 'freq': X = torch.fft.rfft2(X)
                X = X.reshape(xsize[0], -1)
                # X_mean = X.mean(0)
                # X -= X_mean
                X -= self.PCA.mean_


            # To PCA basis
            X = torch.matmul(X, self.PCA.V)

            # Filtering
            if self.last_step: self.Xbffltr = X.t().detach().cpu().numpy()
            X = torch.matmul(X, self.S_gamma)
            if self.cfg.MULTIPLICATOR: X = torch.matmul(X, self.modulator)
            if self.last_step: self.Xaffltr = X.t().detach().cpu().numpy()

            # To original basis
            X = torch.matmul(X, self.PCA.Vt)



            # Shift and reshape
            if self.typePCA == 'channel':
                X += self.PCA.mean_.unsqueeze(1)
                X.reshape(xsize).permute((3, 0, 1, 2))
            else:
                X += self.PCA.mean_
                # X += X_mean

                X = X.reshape(xsize)
                if self.cfg['data_type'] == 'freq': X = torch.fft.ifft2(X)
        return X

    @torch.no_grad()
    def clamp(self):
        self.gamma.data = torch.clamp(self.gamma.data.abs(), min=self.gamma_clamp)

    def enable_register(self):
        self.register_flag = True # register_output
        print('Careful: register_flag is now enabled.')

    def compute_pca(self):  # Ici PCA en fin d'epoch, mieux avec IPCA ?
        if self.typePCA == 'channel':
            assert self.covariance is not None, "self.covariance is empty. Make sure to collect the activations (usually " \
                                                 "at last epoch) "
            self.PCA.fit(A=self.covariance)
        else:
            assert self.activations is not None, "self.activations is empty. Make sure to collect the activations (usually " \
                                                 "at last epoch) "
            if self.cfg['data_type'] == 'freq': self.activations = torch.fft.rfft2(self.activations)
            self.PCA.fit(A=self.activations)

        if self.init_gamma_max:
            self.gamma_init = self.PCA.explained_variance_.max()

        # self.gamma_init = self.PCA.explained_variance_.quantile(self.initquantile) #1e-4*self.PCA.explained_variance_.min()

        self.reset_gamma()
        self.activations = None
        self.covariance = None


    def reset_gamma(self):
        if self.gamma_diag:
            self.gamma.data = torch.full_like(self.PCA.singular_values_, self.gamma_clamp).cuda()
            if self.cfg.MULTIPLICATOR:
                self.mult.data = torch.full_like(self.PCA.singular_values_, 1.).cuda()
        else:
            self.gamma.data = torch.Tensor([self.gamma_clamp]).cuda()
            if self.cfg.MULTIPLICATOR:
                self.mult.data = torch.Tensor([1.]).cuda()


def collect_params(model):
    """Collect the parameters of the GammaLayers.

    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.

    Note: other choices of parameterization are possible!
    """
    params = []
    names = []
    for nm, m in model.named_modules():
        if isinstance(m, GammaLayer):
            for np, p in m.named_parameters():
                params.append(p)
                names.append(f"{nm}.{np}")
    return params, names


class ModuledNet(nn.Module):
    def __init__(self, model, cfg):
        super(ModuledNet, self).__init__()
        # TODO: improve GammaLayer position construction
        #self.gamma_layer_2 = GammaLayer()
        if cfg.MODEL.conv1.ENABLED:
            self.conv1 = nn.Sequential(*[model.conv1, GammaLayer(cfg=cfg.MODEL.conv1)])
        else:
            self.conv1 = model.conv1
        if cfg.MODEL.block1.ENABLED:
            self.block1 = nn.Sequential(*[model.block1, GammaLayer(cfg=cfg.MODEL.block1)])
        else:
            self.block1 = model.block1
        if cfg.MODEL.block2.ENABLED:
            self.block2 = nn.Sequential(*[model.block2, GammaLayer(cfg=cfg.MODEL.block2)])
        else:
            self.block2 = model.block2
        if cfg.MODEL.block3.ENABLED:
            self.block3 = nn.Sequential(*[model.block3, GammaLayer(cfg=cfg.MODEL.block3)])
        else:
            self.block3 = model.block3
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc,
            model.nChannels
        )
        if cfg.MODEL.CONVLIST.ENABLED:
            L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.Conv2d)][1:] #model.conv1 special
            for i in range(10):
                remove = '.' + str(i) + '.'
                replace = '[' + str(i) + '].'
                for subind, sub in enumerate(L):
                    if remove in sub: L[subind] = sub[:sub.find(remove)] + replace + sub[sub.find(remove) + len(remove):]
            for i in cfg.MODEL.CONVLIST.LIST:
                glayer = GammaLayer(cfg=cfg.MODEL.CONVLIST)
                glayer.list_index = i
                exec(f"{L[i]} = nn.Sequential(*[{L[i]}, glayer])")

        # if cfg.MODEL.BN.ENABLED:
        #     L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.BatchNorm2d)]
        #     for i in range(10):
        #         remove = '.' + str(i) + '.'
        #         replace = '[' + str(i) + '].'
        #         for subind, sub in enumerate(L):
        #             if remove in sub: L[subind] = sub[:sub.find(remove)] + replace + sub[sub.find(remove) + len(remove):]
        #     for i in range(len(L)):
        #         exec(f"{L[i]} = nn.Sequential(*[{L[i]}, GammaLayer(gamma_diag = cfg.MODEL.BN.DIAG, channel = cfg.MODEL.BN.TYPE_PCA, filter = cfg.MODEL.BN.FILTER)])")

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn
        self.relu = relu
        self.fc = fc
        self.nChannels = nChannels

    def forward(self, x):
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out)
