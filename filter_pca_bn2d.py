from copy import deepcopy
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import collections
import gc


class FilterPCA2D(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """
    def __init__(self, model, optimizer, steps=1, episodic=False):
        super(FilterPCA2D, self).__init__()
        self.model = model

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d): 
                m.train()

        self.optimizer = optimizer
        self.steps = steps
        assert steps > 0, "tent requires >= 1 step(s) to forward and update"
        self.episodic = episodic

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state, self.optimizer_state = \
            copy_model_and_optimizer(self.model, self.optimizer)

    def forward(self, x):
        if self.episodic:
            self.reset()

        for _ in tqdm(range(self.steps)):
            outputs = forward_and_adapt(x, self.model, self.optimizer)

        for m in self.modules():
            if isinstance(m, GammaLayerBN2D):
                pass
                S_gamma = m.S_gamma.diag()
                print('Gamma', m.gamma)
                print(S_gamma)

        return outputs

    def reset(self):
        if self.model_state is None or self.optimizer_state is None:
            raise Exception("cannot reset without saved model/optimizer state")
        load_model_and_optimizer(self.model, self.optimizer,
                                 self.model_state, self.optimizer_state)


@torch.jit.script
def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


@torch.enable_grad()  # ensure grads in possible no grad context for testing
def forward_and_adapt(x, model, optimizer):
    """Forward and adapt model on batch of data.

    Measure entropy of the model prediction, take gradients, and update params.
    """
    # forward
    outputs = model(x)
    # adapt
    loss = softmax_entropy(outputs).mean(0)
    print(loss)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    return outputs


class GammaLayerBN2D(nn.Module):
    def __init__(
            self,  
            enabled=False, 
            whiten=False, 
            gamma_diag=True
        ):
        
        super(GammaLayerBN2D, self).__init__()
        
        self.gamma_diag = gamma_diag

        self.mean = None 
        self.covariance = None 
        self.momentum = 0.01

        self.register_flag = False
        self.test_time_adaptation = False
        
        self.S_gamma = None
        self.explained_variance = None
        self.V = None
        self.mean = None

        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        
        self.gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        

    def forward(self, X):  # Input (N, C, H, W) => (N*H*W, C) => Filtering => Output (N, C, H, W)
        if self.register_flag:
            with torch.no_grad():
                # Permute and reshape
                xsize = X.size()
                X = X.permute([0, 2, 3, 1])
                permute_size = X.size()
                X = X.reshape(-1, xsize[1])

                if self.covariance is None:
                    self.mean = X.mean(0)
                    self.covariance = torch.matmul(X.t(), X)
                else: 
                    self.mean = (1.-self.momentum)*self.mean + self.momentum * X.mean(0)
                    self.covariance = (1.-self.momentum)*self.covariance + self.momentum * torch.matmul(X.t(), X)

                X = X.reshape(permute_size)
                X = X.permute([0, 3, 1, 2])

        elif self.test_time_adaptation:
            assert hasattr(self, 'V'), "V is not defined. Make sure to self.compute_pca()"
            assert self.gamma.requires_grad

            self.S_gamma = self.explained_variance / (self.explained_variance + F.relu(self.gamma)) 
            
            self.S_gamma = self.S_gamma.diag()

            xsize = X.size()

            # Permute dim 
            X = X.permute([0, 2, 3, 1])
            permute_size = X.size()

            # Reshape and center
            X = X.reshape(-1, xsize[1])
            X -= self.mean

            # To PCA basis
            X = torch.matmul(X, self.V)

            # Filtering
            X = torch.matmul(X, self.S_gamma)

            # To original basis
            X = torch.matmul(X, self.V.t())

            # Shift and reshape
            X += self.mean

            X = X.reshape(permute_size)

            # Permute dim 
            X = X.permute([0, 3, 1, 2])

        return X

    @torch.no_grad()
    def clamp(self):
        self.gamma.data = torch.clamp(self.gamma.data.abs(), min=self.gamma_clamp)

    def register_output(self):
        self.register_flag = True
        print('Careful: register_flag is now enabled.')

    def compute_pca(self):  # Ici PCA en fin d'epoch, mieux avec IPCA ?
        self.register_flag = False
        
        # SVD decomposition
        with torch.no_grad():
            U, self.explained_variance, self.V = torch.svd(self.covariance)  # PCA

        self.explained_variance = self.explained_variance / self.explained_variance.max()

        self.mean = self.mean.cuda()
        self.V = self.V.cuda()
        self.explained_variance = self.explained_variance.cuda()

        self.gamma_init = 1e-12 #self.explained_variance.quantile(0.95) #1e-8 #self.explained_variance.median()
        self.reset_gamma()

        self.test_time_adaptation = True

    def reset_gamma(self):
        if self.gamma_diag:
            self.gamma.data = torch.Tensor([self.gamma_init]*self.explained_variance.size(0)).cuda()
        else:
            self.gamma.data = torch.Tensor([self.gamma_init]).cuda()


def configure_model(model, gamma_diag):
    """Configure model for use with filter_pca."""

    model = GammaNet(model, gamma_diag)
    model.eval()

    # disable grad, to (re-)enable only what tent updates
    model.requires_grad_(False)
    # configure norm for tent updates: enable grad + force batch statisics
    
    for m in model.modules():
        if isinstance(m, GammaLayerBN2D):
            m.requires_grad_(True)

    
    return model


def collect_params(model):
    """Collect the affine scale + shift parameters from batch norms.

    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.

    Note: other choices of parameterization are possible!
    """
    params = []
    names = []
    for nm, m in model.named_modules():
        if isinstance(m, GammaLayerBN2D):
            for np, p in m.named_parameters():
                params.append(p)
                names.append(f"{nm}.{np}")
    return params, names

def load_model_and_optimizer(model, optimizer, model_state, optimizer_state):
    """Restore the model and optimizer states from copies."""
    model.load_state_dict(model_state, strict=True)
    optimizer.load_state_dict(optimizer_state)

def copy_model_and_optimizer(model, optimizer):
    """Copy the model and optimizer states for resetting after adaptation."""
    model_state = deepcopy(model.state_dict())
    optimizer_state = deepcopy(optimizer.state_dict())
    return model_state, optimizer_state

class GammaNet(nn.Module):
    def __init__(self, model, gamma_diag):
        super(GammaNet, self).__init__()
        
        self.gamma_diag = gamma_diag

        self.conv1 = model.conv1
        self.block1 = model.block1
        #self.block1.layer[1].bn1 = nn.Sequential(self.block1.layer[1].bn1, GammaLayerBN2D(gamma_diag=gamma_diag))
        self.block2 = model.block2
        self.block3 = model.block3
        #self.block3.layer[1].bn1 = nn.Sequential(self.block3.layer[1].bn1, GammaLayerBN2D(gamma_diag=gamma_diag))
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc, 
            model.nChannels
        )

        L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.BatchNorm2d)]
        LL = []
        for i in range(10):
            remove = '.' + str(i) + '.'
            replace = '[' + str(i) + '].'
            for sub in L:
                if remove in sub:
                    sub = sub[:sub.find(remove)] + replace + sub[sub.find(remove) + len(remove):]
                    LL.append(sub)
        for i in range(len(LL)):
            exec(f'{LL[i]} = nn.Sequential(*[{LL[i]}, GammaLayerBN2D(gamma_diag=gamma_diag)])')

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn 
        self.relu = relu 
        self.fc = fc 
        self.nChannels = nChannels

    def forward(self, x): 
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out) 

