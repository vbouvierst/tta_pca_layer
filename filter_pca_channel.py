import pdb
from copy import deepcopy
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import collections
import gc


class FilterPCAChannel(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """
    def __init__(self, model, optimizer, steps=1, episodic=False):
        super(FilterPCAChannel, self).__init__()
        self.model = model

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d): 
                m.train()

        self.optimizer = optimizer
        self.steps = steps
        assert steps > 0, "tent requires >= 1 step(s) to forward and update"
        self.episodic = episodic

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state, self.optimizer_state = \
            copy_model_and_optimizer(self.model, self.optimizer)

    def forward(self, x):
        if self.episodic:
            self.reset()

        for _ in range(self.steps):
            outputs = forward_and_adapt(x, self.model, self.optimizer)

        #for m in self.modules():
        #    if isinstance(m, GammaLayerChannel):
        #        print('Gamma', m.gamma)
        #        print(m.S_gamma)

        return outputs

    def reset(self):
        if self.model_state is None or self.optimizer_state is None:
            raise Exception("cannot reset without saved model/optimizer state")
        load_model_and_optimizer(self.model, self.optimizer,
                                 self.model_state, self.optimizer_state)


@torch.jit.script
def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


@torch.enable_grad()  # ensure grads in possible no grad context for testing
def forward_and_adapt(x, model, optimizer):
    """Forward and adapt model on batch of data.

    Measure entropy of the model prediction, take gradients, and update params.
    """
    # forward
    outputs = model(x)
    # adapt
    loss = softmax_entropy(outputs).mean(0)
    print(loss)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    return outputs


class GammaLayerChannel(nn.Module):
    def __init__(
            self,  
            gamma_diag=False,
            enabled=False, 
            whiten=False
        ):
        
        super(GammaLayerChannel, self).__init__()

        self.gamma_diag = gamma_diag
        
        self.mean = None
        self.covariance = None
        self.momentum = 0.01

        self.register_flag = False
        self.test_time_adaptation = False
        
        self.S_gamma = {}
        self.explained_variance = None
        self.V = None
        self.V_t = None 
        self.mean = {}

        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        
        self.gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())

        self.channels = None 

    def init_channels(self, X):
        self.channels = X.shape[1]
        self.covariance = None
        self.mean = None
        self.gamma = nn.parameter.Parameter(torch.Tensor(
            [self.gamma_init] #[self.gamma_init]*self.channels
            ).cuda())

    def forward(self, X):
        if self.channels is None: 
            self.init_channels(X)

        if self.register_flag:
            with torch.no_grad():
                # Permute and reshape
                Xp = X.permute((1,2,3,0)).flatten(1,2)
                N = Xp.shape[2]
                m1 = Xp - Xp.sum(dim=2, keepdim=True) / N
                cov = torch.einsum('ijk,ilk->ijl', m1, m1) / (N - 1)
                means = Xp.mean(2)
                if self.covariance is None:
                    self.mean = means
                    self.covariance = cov
                else:
                    self.mean = (1.-self.momentum)*self.mean + self.momentum * means
                    self.covariance = (1.-self.momentum)*self.covariance + self.momentum * cov

        elif self.test_time_adaptation:
            assert hasattr(self, 'V'), "V is not defined. Make sure to self.compute_pca()"
            assert self.gamma.requires_grad

            xsize = X.size()

            self.S_gamma = self.explained_variance / (self.explained_variance + F.relu(self.gamma)) #.reshape(-1, 1))

            # Reshape and center
            X = X.reshape(self.channels, xsize[0], -1)
            X -= self.mean.unsqueeze(1)

            # To PCA basis
            X = torch.matmul(X, self.V)

            # Filtering
            X = X*self.S_gamma.unsqueeze(1)

            # To original basis
            X = torch.matmul(X, self.V_t)

            # Shift and reshape
            X += self.mean.unsqueeze(1)
            X = X.reshape([xsize[0], self.channels, xsize[2], xsize[3]])
        return X

    @torch.no_grad()
    def clamp(self):
        self.gamma.data = torch.clamp(self.gamma.data.abs(), min=self.gamma_clamp)

    def register_output(self):
        self.register_flag = True
        print('Careful: register_flag is now enabled.')

    def compute_pca(self):  # Ici PCA en fin d'epoch, mieux avec IPCA ?
        self.register_flag = False
        
        # SVD decomposition
        with torch.no_grad():
            _, self.explained_variance, self.V = torch.linalg.svd(self.covariance, full_matrices=False)  # PCA
            self.V_t = self.V.permute((0,2,1))

            self.V = self.V.cuda()
            self.V_t = self.V_t.cuda()

            #for c in range(self.channels):
            #    self.explained_variance[c] = self.explained_variance[c] / self.explained_variance[c].max()


            print('Shape after PCA')
            print(self.explained_variance.shape)
            print(self.V.shape)
            
            

        self.gamma_init = 1e-12 #self.explained_variance.quantile(0.95) #1e-8 #self.explained_variance.median()
        self.reset_gamma()
        self.test_time_adaptation = True

        self.mean = torch.cat([self.mean[c].unsqueeze(0) for c in range(self.channels)], 0).cuda()

    def reset_gamma(self):
        self.gamma = nn.parameter.Parameter(torch.Tensor(
            [self.gamma_init] #[self.gamma_init]*self.channels
            ).cuda())


def configure_model(model, gamma_diag):
    """Configure model for use with filter_pca."""

    model = GammaNet(model, gamma_diag)
    model.eval()

    # disable grad, to (re-)enable only what tent updates
    model.requires_grad_(False)
    # configure norm for tent updates: enable grad + force batch statisics
    
    for m in model.modules():
        if isinstance(m, GammaLayerChannel):
            m.requires_grad_(True)
    return model


def collect_params(model):
    """Collect the affine scale + shift parameters from batch norms.

    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.

    Note: other choices of parameterization are possible!
    """
    params = []
    names = []
    for nm, m in model.named_modules():
        if isinstance(m, GammaLayerChannel):
            for np, p in m.named_parameters():
                params.append(p)
                names.append(f"{nm}.{np}")
    return params, names

def load_model_and_optimizer(model, optimizer, model_state, optimizer_state):
    """Restore the model and optimizer states from copies."""
    model.load_state_dict(model_state, strict=True)
    optimizer.load_state_dict(optimizer_state)

def copy_model_and_optimizer(model, optimizer):
    """Copy the model and optimizer states for resetting after adaptation."""
    model_state = deepcopy(model.state_dict())
    optimizer_state = deepcopy(optimizer.state_dict())
    return model_state, optimizer_state

class GammaNet(nn.Module):
    def __init__(self, 
                model, 
                gamma_diag):
        super(GammaNet, self).__init__()
        
        self.gamma_diag = gamma_diag

        self.conv1 = model.conv1
        self.block1 = model.block1
        self.block2 = model.block2
        self.block3 = model.block3
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc, 
            model.nChannels
        )

        if True:
            L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.BatchNorm2d)]
            LL = []
            for i in range(10):
                remove = '.' + str(i) + '.'
                replace = '[' + str(i) + '].'
                for sub in L:
                    if remove in sub:
                        sub = sub[:sub.find(remove)] + replace + sub[sub.find(remove) + len(remove):]
                        LL.append(sub)
            for i in range(len(LL)):
                exec(f'{LL[i]} = nn.Sequential(*[{LL[i]}, GammaLayerChannel(gamma_diag=gamma_diag)])')

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn 
        self.relu = relu 
        self.fc = fc 
        self.nChannels = nChannels

    def forward(self, x): 
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out) 

