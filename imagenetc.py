import gc
import logging
import pdb
import glob

import torch
import torch.nn as nn 
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim

from robustbench.data import load_imagenetc, load_imagenet
from robustbench.model_zoo.enums import ThreatModel
from robustbench.utils import load_model
from robustbench.utils import clean_accuracy as accuracy

import tent
import norm
import filter_pca
import polar_filter_pca
import discorr
from discorr import DisCorr, Corrlayer
# import filter_pca_bn2d
# from pca import incr_filter_pca
# import filter_pca_channel
from tqdm import tqdm
import sys
import os
import wandb
import numpy as np

from conf import cfg, load_cfg_fom_args

logger = logging.getLogger(__name__)


def evaluate(description):
    assert cfg.CORRUPTION.DATASET.lower() == 'imagenet'
    load_cfg_fom_args(description)
    # configure model
    base_model = load_model(cfg.MODEL.ARCH, cfg.CKPT_DIR,
                       cfg.CORRUPTION.DATASET, ThreatModel.corruptions).cuda()
    run = wandb.init(project="TTAwPCA", entity='notamment', config=cfg)
    run.name = cfg.LOG_DEST.split(os.sep)[-1]
    print('MODEL LOADED')
    if cfg.MODEL.ADAPTATION == "source":
        logger.info("test-time adaptation: NONE")
        model = setup_source(base_model)
    if cfg.MODEL.ADAPTATION == "norm":
        logger.info("test-time adaptation: NORM")
        model = setup_norm(base_model)
    if cfg.MODEL.ADAPTATION == "tent":
        logger.info("test-time adaptation: TENT")
        model = setup_tent(base_model)
    if cfg.MODEL.ADAPTATION == "TTAwPCA":
        logger.info("test-time adaptation with PCA")
        model = setup_TTAwPCA(base_model)
    if cfg.MODEL.ADAPTATION == "DisCorr":
        logger.info("Disentangling with correlations")
        model = setup_DisCorr(base_model)
    if cfg.MODEL.ADAPTATION == "polar_filter_pca":
        logger.info("test-time adaptation with PCA on polar data")
        model = setup_polar_filter_pca(base_model)

    logger.info(f"Number of parameters: {count_parameters(model)}")
    wandb.watch(model)

    if 'no_corruption' in cfg.CORRUPTION.TYPE:
        if cfg.MODEL.ADAPTATION in ["TTAwPCA", "polar_filter_pca"]: model.plot = False
        run.define_metric(f"no_corruption_error", summary="min")
        cfg.CORRUPTION.TYPE.remove('no_corruption')
        model.corruption = 'no_corruption'
        x_test, y_test = load_imagenet(n_examples=cfg.CORRUPTION.NUM_EX, data_dir=cfg.DATA_DIR)
        x_test, y_test = x_test.cuda(), y_test.cuda()
        acc = accuracy(model, x_test, y_test, cfg.TEST.BATCH_SIZE)
        err = 1. - acc
        logger.info(f"error % [no_corruption]: {err:.2%}")
        run.log({f"no_corruption_error": err})

    # evaluate on each severity and type of corruption in turn
    for severity in cfg.CORRUPTION.SEVERITY:
        run.define_metric(f"mean{severity}_error", summary="min")
        L_err = []
        for corruption_type in cfg.CORRUPTION.TYPE:
            run.define_metric(f"{corruption_type}{severity}_error", summary="min")
            # reset adaptation for each combination of corruption x severity
            # note: for evaluation protocol, but not necessarily needed
            model.corruption = corruption_type+str(severity)
            if cfg.MODEL.ADAPTATION in ["TTAwPCA", "polar_filter_pca"]: model.plot = False
            try:
                model.reset()
                logger.info("resetting model")
            except:
                logger.warning("not resetting model")

            x_test, y_test = load_imagenetc(n_examples=cfg.CORRUPTION.NUM_EX,
                                           severity=severity, data_dir=cfg.DATA_DIR, shuffle=False,
                                           corruptions=[corruption_type])
            x_test, y_test = x_test.cuda(), y_test.cuda()
            acc = accuracy(model, x_test, y_test, cfg.TEST.BATCH_SIZE)
            err = 1. - acc
            L_err.append(err)
            logger.info(f"error % [{corruption_type}{severity}]: {err:.2%}")
            run.log({f"{corruption_type}{severity}_error": err})
        run.log({f"mean{severity}_error": np.asarray(L_err).mean()})


def setup_source(model):
    """Set up the baseline source model without adaptation."""
    model.eval()
    logger.info(f"model for evaluation: %s", model)
    return model


def setup_norm(model):
    """Set up test-time normalization adaptation.

    Adapt by normalizing features with test batch statistics.
    The statistics are measured independently for each batch;
    no running average or other cross-batch estimation is used.
    """
    norm_model = norm.Norm(model)
    logger.info(f"model for adaptation: %s", model)
    stats, stat_names = norm.collect_stats(model)
    logger.info(f"stats for adaptation: %s", stat_names)
    return norm_model


def setup_tent(model):
    """Set up tent adaptation.

    Configure the model for training + feature modulation by batch statistics,
    collect the parameters for feature modulation by gradient optimization,
    set up the optimizer, and then tent the model.
    """
    model = tent.configure_model(model)
    params, param_names = tent.collect_params(model)
    optimizer = setup_optimizer(params)
    tent_model = tent.Tent(model, optimizer,
                           steps=cfg.OPTIM.STEPS,
                           episodic=cfg.MODEL.EPISODIC)
    logger.info(f"model for adaptation: %s", model)
    logger.info(f"params for adaptation: %s", param_names)
    logger.info(f"optimizer for adaptation: %s", optimizer)
    return tent_model

def setup_DisCorr(model):
    """Set up DisCorr."""

    model = discorr.configure_model(model=model, cfg=cfg)

    # Collect training data of the model

    transform = transforms.Compose(
        [
            transforms.ToTensor()
        ]
    )

    dataset = torchvision.datasets.CIFAR10(
        cfg.DATA_DIR,
        train=True,
        download=True,
        transform=transform
    )

    loader = torch.utils.data.DataLoader(dataset, batch_size=250, shuffle=True, num_workers=8)

    # Compute PCA for all GammaLayer
    with torch.no_grad():
        for n, m in model.named_modules():
            if isinstance(m, Corrlayer):
                m.name = n
                m.savefolder = cfg.SAVE_DIR
                if os.path.exists(os.path.join(cfg.SAVE_DIR, n+'invcholesLU.pt')) and m.METHOD == 'LU':
                    logger.info(f"loading {os.path.join(cfg.SAVE_DIR, n+'invcholesLU.pt')}")
                    m.invcholes = torch.load(os.path.join(cfg.SAVE_DIR, 'resources', n+'invcholesLU.pt'))
                    m.train_std = torch.load(os.path.join(cfg.SAVE_DIR, 'resources', n + 'train_std.pt'))
                if os.path.exists(os.path.join(cfg.SAVE_DIR, n+'invcholesPCA.pt')) and m.METHOD == 'PCA':
                    logger.info(f"loading {os.path.join(cfg.SAVE_DIR, n + 'invcholesPCA.pt')}")
                    m.invcholes = torch.load(os.path.join(cfg.SAVE_DIR, 'resources', n+'invcholesPCA.pt'))
                    m.train_std = torch.load(os.path.join(cfg.SAVE_DIR, 'resources', n + 'train_std.pt'))
                else:
                    m.enable_register()
                    # Collect training data
                    for i, (x, _) in tqdm(enumerate(loader), file=sys.stdout):
                        x = x.cuda()
                        model(x)
                    m.prepare_disentangle()

    for n, m in model.named_modules():
        if isinstance(m, Corrlayer):
            if not(os.path.exists(os.path.join(cfg.SAVE_DIR, n+'invcholesLU.pt'))) and m.METHOD == 'LU':
                logger.info(f"saving {os.path.join(cfg.SAVE_DIR, n + 'invcholesLU.pt')}")
                torch.save(m.invcholes, os.path.join(cfg.SAVE_DIR, 'resources', n+'invcholesLU.pt'))
            if not(os.path.exists(os.path.join(cfg.SAVE_DIR, n+'invcholesPCA.pt'))) and m.METHOD == 'PCA':
                logger.info(f"saving {os.path.join(cfg.SAVE_DIR, n + 'invcholesPCA.pt')}")
                torch.save(m.invcholes, os.path.join(cfg.SAVE_DIR, 'resources', n+'invcholesPCA.pt'))
            if not (os.path.exists(os.path.join(cfg.SAVE_DIR, n + 'train_std.pt'))):
                torch.save(m.train_std, os.path.join(cfg.SAVE_DIR, 'resources', n + 'train_std.pt'))
            m.register_flag = False
            m.test_time_adaptation = True
        if isinstance(m, nn.BatchNorm2d):
            m.train()
            logger.info(f"Batchnorm {n} set at train")

    optimizer = None

    filter_model = DisCorr(
        model,
        optimizer,
        steps=cfg.OPTIM.STEPS,
        episodic=cfg.MODEL.EPISODIC
    )

    logger.info(f"model for adaptation: {filter_model}")
    logger.info(f"optimizer for adaptation: {optimizer}")

    return filter_model


def setup_polar_filter_pca(model):
    """Set up polar_filter_pca.

    Add PCA layer to the existing layers of the fully trained model,
    collect the parameters for PCA filtering by gradient optimization regarding the entropy,
    set up the optimizer, and then TTAwPCA the model.
    """
    model = polar_filter_pca.configure_model(model=model, cfg=cfg)

    # Collect training data of the model

    transform = transforms.Compose(
        [
            transforms.ToTensor()
        ]
    )

    dataset = torchvision.datasets.CIFAR10(
        cfg.DATA_DIR,
        train=True,
        download=True,
        transform=transform
    )

    loader = torch.utils.data.DataLoader(dataset, batch_size=250, shuffle=True, num_workers=8)
    # logger.info('IMPORTANT : recoloring: "X -= X.mean(0); X += self.PCA.mean_"')

    # Compute PCA for all GammaLayer
    with torch.no_grad():
        for n, m in model.named_modules():
            if isinstance(m, polar_filter_pca.GammaLayer):
                m.name = n
                ckpt_files = glob.glob(os.path.join(cfg.SAVE_DIR, 'resources', n + '*'))
                not_saved = True
                for ckpt_path in ckpt_files:
                    ckpt = torch.load(ckpt_path)
                    if type(ckpt) == type(dict()) and 'cfg' in ckpt.keys() and m.cfg['ENABLED'] == ckpt['cfg']['ENABLED'] and m.cfg['TYPE_PCA'] == ckpt['cfg']['TYPE_PCA'] and ((m.cfg['data_type'] == 'spat' and 'data_type' not in ckpt['cfg']) or ('data_type' in ckpt['cfg'] and m.cfg['data_type'] == ckpt['cfg']['data_type'])):
                        logger.info(f'loading layer {n} from {ckpt_path}')
                        m.PCA_mgn.rank = ckpt['PCA_mgn_rank']
                        m.PCA_mgn.V = ckpt['PCA_mgn_V']
                        m.PCA_mgn.Vt = ckpt['PCA_mgn_Vt']
                        m.PCA_mgn.mean_ = ckpt['PCA_mgn_mean_']
                        m.PCA_mgn.singular_values_ = ckpt['PCA_mgn_singular_values_']
                        m.PCA_mgn.explained_variance_ = ckpt['PCA_mgn_explained_variance_']
                        m.PCA_mgn.explained_variance_ratio_ = ckpt['PCA_mgn_explained_variance_ratio_']
                        m.PCA_phs.rank = ckpt['PCA_phs_rank']
                        m.PCA_phs.V = ckpt['PCA_phs_V']
                        m.PCA_phs.Vt = ckpt['PCA_phs_Vt']
                        m.PCA_phs.mean_ = ckpt['PCA_phs_mean_']
                        m.PCA_phs.singular_values_ = ckpt['PCA_phs_singular_values_']
                        m.PCA_phs.explained_variance_ = ckpt['PCA_phs_explained_variance_']
                        m.PCA_phs.explained_variance_ratio_ = ckpt['PCA_phs_explained_variance_ratio_']
                        m.reset_gamma()
                        not_saved = False
                if not_saved:
                    m.enable_register()
                    logger.info(f"PCA of {n} for adaptation: {m.typePCA}")
                    k=0
                    # Collect training data
                    for i, (x, _) in tqdm(enumerate(loader), file=sys.stdout):
                        x = x.cuda()
                        model(x)
                        if m.typePCA == 'incremental' and (i + 1) * loader.batch_size // 10000 > k:
                            m.compute_pca()
                            k += 1
                    if m.typePCA != 'incremental':
                        m.compute_pca()
                    torch.save({'cfg': m.cfg,
                                'PCA_mgn_rank': m.PCA_mgn.rank,
                                'PCA_mgn_V':m.PCA_mgn.V,
                                'PCA_mgn_Vt':m.PCA_mgn.Vt,
                                'PCA_mgn_mean_':m.PCA_mgn.mean_,
                                'PCA_mgn_singular_values_':m.PCA_mgn.singular_values_,
                                'PCA_mgn_explained_variance_':m.PCA_mgn.explained_variance_,
                                'PCA_mgn_explained_variance_ratio_':m.PCA_mgn.explained_variance_ratio_,
                                'PCA_phs_rank': m.PCA_phs.rank,
                                'PCA_phs_V': m.PCA_phs.V,
                                'PCA_phs_Vt': m.PCA_phs.Vt,
                                'PCA_phs_mean_': m.PCA_phs.mean_,
                                'PCA_phs_singular_values_': m.PCA_phs.singular_values_,
                                'PCA_phs_explained_variance_': m.PCA_phs.explained_variance_,
                                'PCA_phs_explained_variance_ratio_': m.PCA_phs.explained_variance_ratio_
                                },
                               os.path.join(cfg.SAVE_DIR, 'resources', n + '_v' + str(len(ckpt_files)) +'.pt'))
                    m.register_flag = False

    for n, m in model.named_modules():
        if isinstance(m, polar_filter_pca.GammaLayer):
            m.register_flag = False
            m.test_time_adaptation = True
            m.PCA_mgn.explained_variance_ = m.PCA_mgn.explained_variance_.cuda()
            m.PCA_mgn.mean_ = m.PCA_mgn.mean_.cuda()
            m.PCA_mgn.V = m.PCA_mgn.V.cuda()
            m.PCA_mgn.Vt = m.PCA_mgn.Vt.cuda()
            m.PCA_mgn.singular_values_ = m.PCA_mgn.singular_values_.cuda()
            m.PCA_phs.explained_variance_ = m.PCA_phs.explained_variance_.cuda()
            m.PCA_phs.mean_ = m.PCA_phs.mean_.cuda()
            m.PCA_phs.V = m.PCA_phs.V.cuda()
            m.PCA_phs.Vt = m.PCA_phs.Vt.cuda()
            m.PCA_phs.singular_values_ = m.PCA_phs.singular_values_.cuda()
            logger.info(f"Filter of {n} for adaptation: {m.filter}")
            logger.info(f"Gamma init: {m.gamma_init}")

        elif isinstance(m, nn.BatchNorm2d):
            m.train()
            logger.info(f"Batchnorm {n} set at train")

    params, param_names = polar_filter_pca.collect_params(model)
    optimizer = setup_optimizer(params)

    logger.info(f"mgn frozen")

    filter_model = polar_filter_pca.polar_filter_pca(
        model,
        optimizer,
        steps=cfg.OPTIM.STEPS,
        episodic=cfg.MODEL.EPISODIC
    )

    logger.info(f"model for adaptation: {filter_model}")
    logger.info(f"params for adaptation: {param_names}")
    logger.info(f"optimizer for adaptation: {optimizer}")

    return filter_model



def setup_TTAwPCA(model):
    """Set up TTAwPCA.

    Add PCA layer to the existing layers of the fully trained model,
    collect the parameters for PCA filtering by gradient optimization regarding the entropy,
    set up the optimizer, and then TTAwPCA the model.
    """
    model = filter_pca.configure_model(model=model, cfg=cfg)

    # Collect training data of the model

    transform = transforms.Compose(
        [
            transforms.ToTensor()
        ]
    )

    dataset = torchvision.datasets.CIFAR10(
        cfg.DATA_DIR,
        train=True,
        download=True,
        transform=transform
    )

    loader = torch.utils.data.DataLoader(dataset, batch_size=250, shuffle=True, num_workers=8)
    # logger.info('IMPORTANT : recoloring: "X -= X.mean(0); X += self.PCA.mean_"')

    # Compute PCA for all GammaLayer
    with torch.no_grad():
        for n, m in model.named_modules():
            if isinstance(m, filter_pca.GammaLayer):
                m.name = n
                ckpt_files = glob.glob(os.path.join(cfg.SAVE_DIR, 'resources', n + '*'))
                not_saved = True
                for ckpt_path in ckpt_files:
                    ckpt = torch.load(ckpt_path)
                    if type(ckpt) == type(dict()) and 'cfg' in ckpt.keys() and m.cfg['ENABLED'] == ckpt['cfg']['ENABLED'] and m.cfg['TYPE_PCA'] == ckpt['cfg']['TYPE_PCA'] and ((m.cfg['data_type'] == 'spat' and 'data_type' not in ckpt['cfg']) or ('data_type' in ckpt['cfg'] and m.cfg['data_type'] == ckpt['cfg']['data_type'])):
                        logger.info(f'loading layer {n} from {ckpt_path}')
                        m.PCA.rank = ckpt['rank']
                        m.PCA.V = ckpt['V']
                        m.PCA.Vt = ckpt['Vt']
                        m.PCA.mean_ = ckpt['mean_']
                        m.PCA.singular_values_ = ckpt['singular_values_']
                        m.PCA.explained_variance_ = ckpt['explained_variance_']
                        m.PCA.explained_variance_ratio_ = ckpt['explained_variance_ratio_']
                        m.reset_gamma()
                        not_saved = False
                if not_saved:
                    m.enable_register()
                    logger.info(f"PCA of {n} for adaptation: {m.typePCA}")
                    k=0
                    # Collect training data
                    for i, (x, _) in tqdm(enumerate(loader), file=sys.stdout):
                        x = x.cuda()
                        model(x)
                        if m.typePCA == 'incremental' and (i + 1) * loader.batch_size // 10000 > k:
                            m.compute_pca()
                            k += 1
                    if m.typePCA != 'incremental':
                        m.compute_pca()
                    torch.save({'cfg': m.cfg,
                                'rank': m.PCA.rank,
                                'V':m.PCA.V,
                                'Vt':m.PCA.Vt,
                                'mean_':m.PCA.mean_,
                                'singular_values_':m.PCA.singular_values_,
                                'explained_variance_':m.PCA.explained_variance_,
                                'explained_variance_ratio_':m.PCA.explained_variance_ratio_},
                               os.path.join(cfg.SAVE_DIR, 'resources', n + '_v' + str(len(ckpt_files)) +'.pt'))
                    m.register_flag = False

    for n, m in model.named_modules():
        if isinstance(m, filter_pca.GammaLayer):
            m.register_flag = False
            m.test_time_adaptation = True
            m.PCA.explained_variance_ = m.PCA.explained_variance_.cuda()
            if m.variance_norm: m.PCA.explained_variance_ /= m.PCA.explained_variance_.max()
            m.PCA.mean_ = m.PCA.mean_.cuda()
            m.PCA.V = m.PCA.V.cuda()
            m.PCA.Vt = m.PCA.Vt.cuda()
            m.PCA.singular_values_ = m.PCA.singular_values_.cuda()
            logger.info(f"Filter of {n} for adaptation: {m.filter}")
            logger.info(f"Gamma init: {m.gamma_init}")

        elif isinstance(m, nn.BatchNorm2d):
            if cfg.MODEL.BN.RUNNING_STATS:
                eps = 1e-5
                momentum = 0.1
                reset_stats = False
                no_stats = False
                logger.info(f"Batchnorm {n} set at train")
                # use batch-wise statistics in forward
                m.train()
                m.requires_grad_(False)
                # configure epsilon for stability, and momentum for updates
                m.eps = eps
                m.momentum = momentum
                if reset_stats:
                    # reset state to estimate test stats without train stats
                    m.reset_running_stats()
                if no_stats:
                    # disable state entirely and use only batch stats
                    m.track_running_stats = False
                    m.running_mean = None
                    m.running_var = None

    params, param_names = filter_pca.collect_params(model)
    optimizer = setup_optimizer(params)
    if cfg.MODEL.BN.RUNNING_STATS:
        stats, stat_names = norm.collect_stats(model)
        logger.info(f"stats for adaptation: %s", stat_names)

    filter_model = filter_pca.TTAwPCA(
        model,
        optimizer,
        cfg
    )

    logger.info(f"model for adaptation: {filter_model}")
    logger.info(f"params for adaptation: {param_names}")
    logger.info(f"optimizer for adaptation: {optimizer}")

    return filter_model


def setup_optimizer(params):
    """Set up optimizer for tent adaptation.

    Tent needs an optimizer for test-time entropy minimization.
    In principle, tent could make use of any gradient optimizer.
    In practice, we advise choosing Adam or SGD+momentum.
    For optimization settings, we advise to use the settings from the end of
    trainig, if known, or start with a low learning rate (like 0.001) if not.

    For best results, try tuning the learning rate and batch size.
    """
    if cfg.OPTIM.METHOD == 'Adam':
        return optim.Adam(params,
                    lr=cfg.OPTIM.LR,
                    betas=(cfg.OPTIM.BETA, 0.999),
                    weight_decay=cfg.OPTIM.WD)
    elif cfg.OPTIM.METHOD == 'SGD':
        return optim.SGD(params,
                   lr=cfg.OPTIM.LR,
                   momentum=cfg.OPTIM.MOMENTUM,
                   dampening=cfg.OPTIM.DAMPENING,
                   weight_decay=cfg.OPTIM.WD,
                   nesterov=cfg.OPTIM.NESTEROV)
    else:
        raise NotImplementedError


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)



if __name__ == '__main__':
    evaluate('"CIFAR-10-C evaluation.')
