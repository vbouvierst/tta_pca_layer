import pdb
import torch


class DivLoss(torch.nn.modules.loss._Loss):
    """Diversity loss as SHOT or IT+CM"""

    def __init__(self, target_distr: str = 'uniform', size_average=None, reduce=None, reduction: str = 'batchmean', log_target: bool = False, k: float = 0.9) -> None:
        super(DivLoss, self).__init__(size_average, reduce, reduction)
        self.target_distr = target_distr
        self.log_target = log_target
        self.k = k
        self.target_estimate = torch.Tensor([1]).cuda()

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        if self.target_distr == 'uniform':
            target = torch.ones_like(input)
        else:
            raise NotImplementedError
        self.target_estimate = (self.k*self.target_estimate.detach()-(1-self.k)*input.softmax(1))
        return torch.nn.functional.kl_div(self.target_estimate.log(), target, reduction=self.reduction, log_target=self.log_target)

def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)

def hard_likelihood_ratio(x: torch.Tensor) -> torch.Tensor:
    """Hard likelihood ratio loss from logits as defined in https://arxiv.org/pdf/2106.14999.pdf."""
    return (x.max(1)[0]-(x.exp()*(x<x.max(1, keepdim=True)[0]).type(torch.cuda.FloatTensor)).sum(1).log())

def soft_likelihood_ratio(x: torch.Tensor) -> torch.Tensor:
    """Soft likelihood ratio loss from logits."""
    y = x.softmax(1)
    loss = torch.zeros(x.size(0)).cuda()
    for c in range(0, x.size(1)):
        loss += y[:,c]*(torch.cat((x[:,:c], x[:,c:]), 1).exp().sum(1).log()-x[:,c])
    return loss

class Renyi_entropy(torch.nn.modules.loss._Loss):
    """The Renyi entropy is a generalisation of the more well-known Shannon entropy"""

    def __init__(self, size_average=None, reduce=None, reduction: str = 'batchmean', order: float = 1.0) -> None:
        super(Renyi_entropy, self).__init__(size_average, reduce, reduction)
        self.order = order

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        if self.order == 1: # compute the limit: Shannon entropy
            return softmax_entropy(x)
        else:
            return 1/(1-self.order) * x.log_softmax(1).sum(1)
