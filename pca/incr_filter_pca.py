from copy import deepcopy
from tqdm import tqdm
from pca.utils._incremental_pca import IncrementalPCA

import torch
import torch.nn as nn
import torch.nn.functional as F


class IncrFilterPCA(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """
    def __init__(self, model, optimizer, steps=1, episodic=False):
        super(IncrFilterPCA, self).__init__()
        self.model = model

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d): 
                m.train()

        self.optimizer = optimizer
        self.steps = steps
        assert steps > 0, "tent requires >= 1 step(s) to forward and update"
        self.episodic = episodic

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state, self.optimizer_state = \
            copy_model_and_optimizer(self.model, self.optimizer)

    def forward(self, x):
        if self.episodic:
            self.reset()

        for _ in tqdm(range(self.steps)):
            outputs = forward_and_adapt(x, self.model, self.optimizer)

        for m in self.modules():
            if isinstance(m, GammaLayer):
                pass
                S_gamma = m.S_gamma.diag()
                #print('Gamma', m.gamma)
                #print(S_gamma)

        return outputs

    def reset(self):
        if self.model_state is None or self.optimizer_state is None:
            raise Exception("cannot reset without saved model/optimizer state")
        load_model_and_optimizer(self.model, self.optimizer,
                                 self.model_state, self.optimizer_state)


@torch.jit.script
def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


@torch.enable_grad()  # ensure grads in possible no grad context for testing
def forward_and_adapt(x, model, optimizer):
    """Forward and adapt model on batch of data.

    Measure entropy of the model prediction, take gradients, and update params.
    """
    # forward
    outputs = model(x)
    # adapt
    loss = softmax_entropy(outputs).mean(0)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    return outputs


class GammaLayer(nn.Module):
    def __init__(
            self,  
            enabled=False, 
            whiten=False, 
            init_gamma_max=False, 
            gamma_diag=True
        ):
        
        super(GammaLayer, self).__init__()
        
        self.gamma_diag = gamma_diag
        self.register_flag = False
        self.test_time_adaptation = False
        self.activations = None
        self.S_gamma = None
        self.explained_variance = None
        self.singular_values = None
        self.V = None
        self.q = 5000
        self.mean = None
        self.whiten = whiten 
        self.enabled = enabled
        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        
        self.gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        self.init_gamma_max = init_gamma_max

    def register(self):
        self.register_flag = True
        self.PCAfitter = IncrementalPCA(q=self.q)

    def forward(self, X):  # Input (N, C, H, W) => (N, C*H*W)
        if self.register_flag:
            with torch.no_grad():
                self.activations = torch.cat(
                    (
                    self.activations.cpu(), 
                    X.view(X.size(0), -1).cpu()
                    )
                    ) if self.activations is not None else X.view(X.size(0), -1).cpu()
                print(self.activations.shape)
        elif self.test_time_adaptation:
            assert hasattr(self, 'V'), "V is not defined. Make sure to self.compute_pca()"
            assert self.gamma.requires_grad

            #self.clamp()

            self.S_gamma = 1./(1. + (self.gamma**2 - self.explained_variance).exp()) #self.explained_variance / (self.explained_variance + F.relu(self.gamma))    #  #
            self.S_gamma = self.S_gamma.diag()

            xsize = X.size()

            # Reshape and center
            X = X.reshape(xsize[0], -1)
            X -= self.mean

            # To PCA basis
            X = torch.matmul(X, self.V)

            # Filtering
            X = torch.matmul(X, self.S_gamma)

            # To original basis
            X = torch.matmul(X, self.V.t())

            # Shift and reshape
            X += self.mean
            X = X.reshape(xsize)
        return X

    @torch.no_grad()
    def clamp(self):
        self.gamma.data = torch.clamp(self.gamma.data.abs(), min=self.gamma_clamp)

    def compute_pca(self):  # Ici PCA en fin d'epoch, mieux avec IPCA ?
        assert self.activations is not None, "self.activations is empty. Make sure to collect the activations (usually " \
                                            "at last epoch) "
        self.n_samples = self.activations.size(0)
        self.n_features = self.activations.size(1)
        # Incremental PCA
        print('activation size', self.activations.shape)
        print(f'rank: {min(self.q, min(self.activations.shape))}')
        self.PCAfitter.partial_fit(self.activations)

        self.activations = None

        self.mean = self.PCAfitter.mean_.cuda()
        self.V = self.PCAfitter.components_.t().cuda()
        self.explained_variance = self.PCAfitter.explained_variance_.cuda()
        self.singular_values = self.PCAfitter.singular_values_.cuda()

    def reset_gamma(self):
        if self.gamma_diag:
            self.gamma.data = torch.Tensor([self.gamma_clamp]*self.singular_values.size(0)).cuda()
        else:
            self.gamma.data = torch.Tensor([self.gamma_clamp]).cuda()


def configure_model(model, gamma_diag):
    """Configure model for use with filter_pca."""

    model = ModuledNet(model, gamma_diag)
    model.eval()

    # disable grad, to (re-)enable only what tent updates
    model.requires_grad_(False)
    # configure norm for tent updates: enable grad + force batch statisics
    
    for m in model.modules():
        if isinstance(m, GammaLayer):
            m.requires_grad_(True)
    return model


def collect_params(model):
    """Collect the affine scale + shift parameters from batch norms.

    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.

    Note: other choices of parameterization are possible!
    """
    params = []
    names = []
    for nm, m in model.named_modules():
        if isinstance(m, GammaLayer):
            for np, p in m.named_parameters():
                params.append(p)
                names.append(f"{nm}.{np}")
    return params, names

def load_model_and_optimizer(model, optimizer, model_state, optimizer_state):
    """Restore the model and optimizer states from copies."""
    model.load_state_dict(model_state, strict=True)
    optimizer.load_state_dict(optimizer_state)

def copy_model_and_optimizer(model, optimizer):
    """Copy the model and optimizer states for resetting after adaptation."""
    model_state = deepcopy(model.state_dict())
    optimizer_state = deepcopy(optimizer.state_dict())
    return model_state, optimizer_state

class ModuledNet(nn.Module):
    def __init__(self, model, gamma_diag):
        super(ModuledNet, self).__init__()
        #self.gamma_layer_2 = GammaLayer()
        self.conv1 = model.conv1
        self.gamma_conv_1 = GammaLayer(gamma_diag=gamma_diag)
        self.block1 = model.block1
        self.block2 = model.block2
        self.block3 = model.block3
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc, 
            model.nChannels
        )

    def forward(self, x):
        out = self.conv1(x)
        out = self.gamma_conv_1(out)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        #out = self.gamma_layer_2(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn 
        self.relu = relu 
        self.fc = fc 
        self.nChannels = nChannels

    def forward(self, x): 
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out) 
