import pdb

from torch import Tensor
import torch
import numbers
from torch import _linalg_utils as _utils
from torch._lowrank import _svd_lowrank
from pca.pca_base import PCA_base

from typing import Optional, Tuple


def gen_batches(m, batch_size, *, min_batch_size=0):
    """Generator to create slices containing batch_size elements, from 0 to n.
    The last slice may contain less than batch_size elements, when batch_size
    does not divide n.
    Parameters
    ----------
    m : int
    batch_size : int
        Number of element in each batch.
    min_batch_size : int, default=0
        Minimum batch size to produce.
    Yields
    ------
    slice of batch_size elements
    See Also
    --------
    gen_even_slices: Generator to create n_packs slices going up to n.
    """
    if not isinstance(batch_size, numbers.Integral):
        raise TypeError("gen_batches got batch_size=%s, must be an"
                        " integer" % batch_size)
    if batch_size <= 0:
        raise ValueError("gen_batches got batch_size=%s, must be"
                         " positive" % batch_size)
    start = 0
    for _ in range(int(m // batch_size)):
        end = start + batch_size
        if end + min_batch_size > m:
            continue
        yield slice(start, end)
        start = end
    if start < m:
        yield slice(start, m)


def _incremental_mean_and_var(X, last_mean, last_variance, last_sample_count):
    """Calculate mean update and a Youngs and Cramer variance update.
    last_mean and last_variance are statistics computed at the last step by the
    function. Both must be initialized to 0.0. In case no scaling is required
    last_variance can be None. The mean is always required and returned because
    necessary for the calculation of the variance. last_n_samples_seen is the
    number of samples encountered until now.
    From the paper "Algorithms for computing the sample variance: analysis and
    recommendations", by Chan, Golub, and LeVeque.
    Parameters
    ----------
    X : tensor-like of shape (m, n)
        Data to use for variance update.
    last_mean : tensor-like of shape (n_batch,)
    last_variance : tensor-like of shape (n_batch,)
    last_sample_count : tensor-like of shape (n_batch,)
    Returns
    -------
    updated_mean : tensor of shape (n_batch,)
    updated_variance : tensor of shape (n_batch,)
        If None, only mean is computed.
    updated_sample_count : tensor of shape (n_batch,)
    Notes
    -----
    NaNs are ignored during the algorithm.
    References
    ----------
    T. Chan, G. Golub, R. LeVeque. Algorithms for computing the sample
        variance: recommendations, The American Statistician, Vol. 37, No. 3,
        pp. 242-247
    Also, see the sparse implementation of this in
    `utils.sparsefuncs.incr_mean_variance_axis` and
    `utils.sparsefuncs_fast.incr_mean_variance_axis0`
    """
    # old = stats until now
    # new = the current increment
    # updated = the aggregated stats
    last_sum = last_mean * last_sample_count
    new_sum = torch.nansum(X, dim=0) #

    new_sample_count = torch.sum(torch.bitwise_not(torch.isnan(X)), dim=0)
    updated_sample_count = last_sample_count + new_sample_count

    updated_mean = (last_sum + new_sum) / updated_sample_count

    if last_variance is None:
        updated_variance = None
    else:
        new_unnormalized_variance = (torch.var(X, dim=0) * new_sample_count)
        last_unnormalized_variance = last_variance * last_sample_count

        #with np.errstate(divide='ignore', invalid='ignore'):
        last_over_new_count = last_sample_count / new_sample_count
        updated_unnormalized_variance = (
            last_unnormalized_variance + new_unnormalized_variance +
            last_over_new_count / updated_sample_count *
            (last_sum / last_over_new_count - new_sum) ** 2)

        zeros = last_sample_count == 0
        updated_unnormalized_variance[zeros] = new_unnormalized_variance[zeros]
        updated_variance = updated_unnormalized_variance / updated_sample_count

    return updated_mean, updated_variance, updated_sample_count


class IncrementalPCA(PCA_base):
    """Incremental principal components analysis (IPCA).
    Linear dimensionality reduction using Singular Value Decomposition of
    the data, keeping only the most significant singular vectors to
    project the data to a lower dimensional space. The input data is centered
    but not scaled for each feature before applying the SVD.
    Depending on the size of the input data, this algorithm can be much more
    memory efficient than a PCA, and allows sparse input.
    This algorithm has constant memory complexity, on the order
    of ``batch_size * n_batch``, enabling use of np.memmap files without
    loading the entire file into memory. For sparse matrices, the input
    is converted to dense in batches (in order to be able to subtract the
    mean) which avoids storing the entire dense matrix at any one time.
    The computational overhead of each SVD is
    ``O(batch_size * n_batch ** 2)``, but only 2 * batch_size samples
    remain in memory at a time. There will be ``m / batch_size`` SVD
    computations to get the principal components, versus 1 large SVD of
    complexity ``O(m * n ** 2)`` for PCA.
    Parameters
    ----------
    q : int, default=None
        Number of components to keep. If ``q`` is ``None``,
        then ``q`` is set to ``min(m, n)``.
    # whiten : bool, default=False
    #     When True (False by default) the ``components_`` vectors are divided
    #     by ``n_samples`` times ``components_`` to ensure uncorrelated outputs
    #     with unit component-wise variances.
    #     Whitening will remove some information from the transformed signal
    #     (the relative variance scales of the components) but can sometimes
    #     improve the predictive accuracy of the downstream estimators by
    #     making data respect some hard-wired assumptions.
    # copy : bool, default=True
    #     If False, X will be overwritten. ``copy=False`` can be used to
    #     save memory but is unsafe for general use.
    batch_size : int, default=None
        The number of samples to use for each batch. Only used when calling
        ``fit``. If ``batch_size`` is ``None``, then ``batch_size``
        is inferred from the data and set to ``5 * n_batch``, to provide a
        balance between approximation accuracy and memory consumption.
    Attributes
    ----------
    components_ : ndarray of shape (q, n_batch)
        Components with maximum variance.
    explained_variance_ : ndarray of shape (q,)
        Variance explained by each of the selected components.
    explained_variance_ratio_ : ndarray of shape (q,)
        Percentage of variance explained by each of the selected components.
        If all components are stored, the sum of explained variances is equal
        to 1.0.
    singular_values_ : ndarray of shape (q,)
        The singular values corresponding to each of the selected components.
        The singular values are equal to the 2-norms of the ``q``
        variables in the lower-dimensional space.
    mean_ : ndarray of shape (n_features,)
        Per-feature empirical mean, aggregate over calls to ``partial_fit``.
    var_ : ndarray of shape (n_features,)
        Per-feature empirical variance, aggregate over calls to
        ``partial_fit``.
    noise_variance_ : float
        The estimated noise covariance following the Probabilistic PCA model
        from Tipping and Bishop 1999. See "Pattern Recognition and
        Machine Learning" by C. Bishop, 12.2.1 p. 574 or
        http://www.miketipping.com/papers/met-mppca.pdf.
    q : int
        The estimated number of components. Relevant when
        ``q=None``.
    n_samples_seen_ : int
        The number of samples processed by the estimator. Will be reset on
        new calls to fit, but increments across ``partial_fit`` calls.
    batch_size_ : int
        Inferred batch size from ``batch_size``.
    Implements the incremental PCA model from:
    *D. Ross, J. Lim, R. Lin, M. Yang, Incremental Learning for Robust Visual
    Tracking, International Journal of Computer Vision, Volume 77, Issue 1-3,
    pp. 125-141, May 2008.*
    See https://www.cs.toronto.edu/~dross/ivt/RossLimLinYang_ijcv.pdf
    This model is an extension of the Sequential Karhunen-Loeve Transform from:
    *A. Levy and M. Lindenbaum, Sequential Karhunen-Loeve Basis Extraction and
    its Application to Images, IEEE Transactions on Image Processing, Volume 9,
    Number 8, pp. 1371-1374, August 2000.*
    See https://www.cs.technion.ac.il/~mic/doc/skl-ip.pdf
    We have specifically abstained from an optimization used by authors of both
    papers, a QR decomposition used in specific situations to reduce the
    algorithmic complexity of the SVD. The source for this technique is
    *Matrix Computations, Third Edition, G. Holub and C. Van Loan, Chapter 5,
    section 5.4.4, pp 252-253.*. This technique has been omitted because it is
    advantageous only when decomposing a matrix with ``n_samples`` (rows)
    >= 5/3 * ``n_features`` (columns), and hurts the readability of the
    implemented algorithm. This would be a good opportunity for future
    optimization, if it is deemed necessary.
    References
    ----------
    D. Ross, J. Lim, R. Lin, M. Yang. Incremental Learning for Robust Visual
    Tracking, International Journal of Computer Vision, Volume 77,
    Issue 1-3, pp. 125-141, May 2008.
    G. Golub and C. Van Loan. Matrix Computations, Third Edition, Chapter 5,
    Section 5.4.4, pp. 252-253.
    See Also
    --------
    PCA
    KernelPCA
    SparsePCA
    TruncatedSVD
    """ #

    def __init__(self, rank=None, batch_size: Optional[int] = None):
        super().__init__(rank)
        self.batch_size = batch_size
        self.n_samples_seen_ = 0
        self.var_ = 0.

    # def fit(self, A: Tensor, niter: int = 2):
    #
    #     # if not torch.jit.is_scripting(): ## check
    #     #     if type(A) is not torch.Tensor and has_torch_function((A,)):
    #     #         return handle_torch_function(ipca_lowrank, (A,), A, q=q, niter=niter, batch_size=batch_size)
    #
    #     (m, n) = A.shape[-2:]
    #
    #     if self.rank is None:
    #         self.rank = min(5000, m, n)
    #     elif self.rank < 0 or self.rank > min(m, n):
    #         raise ValueError('q(={}) must be non-negative integer'
    #                          ' and not greater than min(m, n)={}'
    #                          .format(self.rank, min(m, n)))
    #     if not (niter >= 0):
    #         raise ValueError('niter(={}) must be non-negative integer'
    #                          .format(niter))
    #
    #     if self.batch_size is None: self.batch_size = 5 * n
    #
    #     for batch in gen_batches(m, self.batch_size, min_batch_size=n or 0): # partial fit
    #         A_batch = A[batch]
    #
    #         if _utils.is_sparse(A_batch): A_batch = A_batch.to_dense()
    #
    #         self.partial_fit(A_batch, niter)
    #
    #     return self
    #
    # def partial_fit(self, A_batch: Tensor, niter: int = 2):
    def fit(self, A: Tensor, niter: int = 2):
        if _utils.is_sparse(A):
            raise TypeError(
                "IncrementalPCA.partial_fit does not support "
                "sparse input. Either convert data to dense "
                "or use IncrementalPCA.fit to do so in batches.")

        m_batch, n_batch = A.shape
        if self.rank is None:
            self.rank = min(1500, m_batch, n_batch)

        if not 1 <= self.rank <= n_batch:
            raise ValueError("rank q=%r invalid for n_batch=%d features, need "
                             "more rows than columns for IncrementalPCA "
                             "processing" % (self.rank, n_batch))

        if not self.rank <= m_batch:
            raise ValueError("rank q=%r must be less or equal to "
                             "the batch number of samples "
                             "%d." % (self.rank, m_batch))



        # Update stats - they are 0 if this is the first stepc
        col_mean, col_var, n_total_samples = _incremental_mean_and_var(A, last_mean=self.mean_, last_variance=self.var_,
                                                                       last_sample_count=torch.Tensor([self.n_samples_seen_]).repeat(A.shape[1]))
        n_total_samples = n_total_samples[0]

        # Whitening
        if self.n_samples_seen_ == 0:
            # If it is the first step, simply whiten X
            A -= col_mean
        else:
            col_batch_mean = torch.mean(A, dim=0)
            A -= col_batch_mean
            # Build matrix of combined previous basis and new data
            mean_correction = torch.sqrt((self.n_samples_seen_ / n_total_samples) * m_batch) * (self.mean_ - col_batch_mean)
            A = torch.vstack((self.singular_values_.reshape((-1, 1)) * self.Vt, A, mean_correction))

        print('activation size', A.shape)
        print(f'rank {self.rank}')
        U, S, V = _svd_lowrank(A, self.rank, niter=niter, M=None)
        print(f'V shape {V.shape}')
        print(f'm_batch {m_batch}')
        print(f'n_batch {n_batch}')
        #U, Vt = svd_flip(U, Vt, u_based_decision=False)
        explained_variance = S ** 2 / (n_total_samples - 1)
        explained_variance_ratio = S ** 2 / torch.sum(col_var * n_total_samples)

        self.n_samples_seen_ = n_total_samples
        self.V = V # V[:m_batch]
        self.Vt = V.t()
        self.singular_values_ = S # S[:m_batch]
        self.mean_ = col_mean
        self.var_ = col_var
        self.explained_variance_ = explained_variance # explained_variance[:m_batch]
        self.explained_variance_ratio_ = explained_variance_ratio # explained_variance_ratio[:m_batch]

        return self