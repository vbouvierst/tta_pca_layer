import pdb

from torch import Tensor
import torch
from pca.pca_base import PCA_base


class PCA(PCA_base):

    def fit(self, A: Tensor, niter: int = 2):
        n_samples = A.size(0)
        n_features = A.size(1)
        effective = False
        if self.rank == 'effective':
            self.rank = min(n_samples, n_features)
            effective = True
        if self.rank is None:
            self.rank = min(n_samples, n_features)

        self.mean_ = torch.mean(A, dim=0)

        # SVD decomposition
        print('activation size', A.shape)
        with torch.no_grad():
            print('Computing singular values')
            # U, self.singular_values_, self.V = torch.linalg.svd(A-self.mean_, full_matrices=False)
            U, self.singular_values_, self.V = torch.pca_lowrank(A, q=self.rank)
            # q = min(self.rank, A.shape[1])
            # self.singular_values_ = self.singular_values_[:q]
            # self.V = self.V[:, :q]

        self.effective_rank = -(self.singular_values_ / self.singular_values_.max() * (
                    self.singular_values_ / self.singular_values_.max()).log()).sum()
        print(self.effective_rank)
        if effective:
            self.rank = self.effective_rank
            self.singular_values_ = self.singular_values_[:self.rank]
            self.V = self.V[:, :self.rank]
        self.explained_variance_ = (self.singular_values_ ** 2) / (n_samples - 1) + torch.finfo(torch.float32).eps  # stability
        self.explained_variance_ratio_ = self.explained_variance_ / self.explained_variance_.sum()
        self.Vt = self.V.t()

        return self