from torch import Tensor

class PCA_base():
    def __init__(self, rank=None):
        super(PCA_base, self).__init__()
        self.rank = rank
        self.V = None
        self.Vt = None
        self.mean_ = 0.
        self.singular_values_ = None
        self.explained_variance_ = None
        self.explained_variance_ratio_ = None

    def fit(self, A: Tensor, niter: int = 2):
        pass