from torch import Tensor
import torch
from pca.pca_base import PCA_base


class SVD(PCA_base):

    def fit(self, A: Tensor, niter: int = 2):
        n_samples = A.size(0)
        n_features = A.size(1)

        if self.rank is None:
            self.rank = min(5000, n_samples, n_features)

        self.mean_ = torch.mean(A, dim=0)
        A -= self.mean_

        # SVD decomposition
        print('activation size', A.shape)
        with torch.no_grad():
        #     print('Computing singular values')
            _, self.singular_values_, self.V = torch.linalg.svd(A, full_matrices=False)  # PCA
            self.Vt = self.V.permute((0, 2, 1))
        self.explained_variance_ = (self.singular_values_ ** 2) / (n_samples - 1) + torch.finfo(torch.float32).eps  # stability
        self.explained_variance_ratio_ = self.explained_variance_ / self.explained_variance_.sum()

        return self