import pdb
from copy import deepcopy
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import collections
import gc
from pca import pca, incremental_pca, svd
import sys
import os
import matplotlib.pyplot as plt


class polar_filter_pca(nn.Module):
    """FilterPCA adapts a model by filtering eigen values during testing.

    Once filtered, a model adapts itself by updating on every forward.
    """

    def __init__(self, model, optimizer, run, steps=1, episodic=False):
        super(polar_filter_pca, self).__init__()
        self.model = model
        self.run = run

        # Enable transductive BatchNorm2D
        for m in self.model.modules():
            if isinstance(m, nn.BatchNorm2d):
                m.train()

        self.optimizer = optimizer
        self.steps = steps
        assert steps > 0, "TTAwPCA requires >= 1 step(s) to forward and update"
        self.episodic = episodic

        # note: if the model is never reset, like for continual adaptation,
        # then skipping the state copy would save memory
        self.model_state, self.optimizer_state = copy_model_and_optimizer(self.model, self.optimizer)

    def forward(self, x):
        if self.episodic:
            self.reset()

        for step in tqdm(range(self.steps), file=sys.stdout):
            if step == self.steps - 1:
                for n in self.model.gammalayers.keys():
                    m = self.model.gammalayers[n]
                    m.last_step = True
            outputs = forward_and_adapt(x, self.model, self.optimizer)

            if (step == self.steps - 1) and self.plot:
                for n in self.model.gammalayers.keys():
                    m = self.model.gammalayers[n]
                    plt.plot(m.Xbffltr)
                    plt.savefig(os.path.join('/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE/filter_study',
                                             f'testplot_{n}{self.corruption}_Xbffltr.png'))
                    plt.close('all')
                    plt.plot(m.Xaffltr)
                    plt.savefig(os.path.join('/gpfs/workdir/cordiert/checkpoints/tta_pca/SAVE/filter_study',
                                             f'testplot_{n}{self.corruption}_Xaffltr.png'))
                    self.plot = False

        # for m in self.modules():
        #     if isinstance(m, GammaLayer):
        #         pass
        #         S_gamma = m.S_gamma.diag()
        #         print('Gamma', m.gamma)
        #         print(S_gamma)

        return outputs

    def reset(self):
        if self.model_state is None or self.optimizer_state is None:
            raise Exception("cannot reset without saved model/optimizer state")
        load_model_and_optimizer(self.model, self.optimizer,
                                 self.model_state, self.optimizer_state)

    @torch.enable_grad()  # ensure grads in possible no grad context for testing
    def forward_and_adapt(self, x, model, optimizer):
        """Forward and adapt model on batch of data.

        Measure entropy of the model prediction, take gradients, and update params.
        """
        # forward
        outputs = model(x)
        # adapt
        loss = softmax_entropy(outputs).mean(0)
        self.run.log({f"{self.corruption}_loss": loss})
        # print(loss)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        return outputs


def configure_model(model, cfg):
    """Configure model for use with filter_pca."""

    model = ModuledNet(model, cfg)
    model.eval()

    model.requires_grad_(False)

    model.gammalayers = {}
    for n, m in model.named_modules():
        if isinstance(m, GammaLayer):
            m.requires_grad_(True)
            model.gammalayers[n] = m
    return model

def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


# @torch.jit.script
# def soft_likelihood_ratio(x: torch.Tensor) -> torch.Tensor:
#     """Soft likelihood ratio loss from logits."""
#     y=x.softmax(1)
#     T_sum_new_x = None
#     for c in range(x.size(1)):
#         new_x = x.exp()
#         new_x[:,c]=0
#         sum_new_x = new_x.sum(1)
#         T_sum_new_x = sum_new_x if T_sum_new_x in None else torch.cat(T_sum_new_x, sum_new_x)
#     return -(y*(y/(y.sum(1,True)-y)).log()).sum(1)



class GammaLayer(nn.Module):
    def __init__(
            self,
            init_gamma_max=False,
            cfg=None
    ):

        super(GammaLayer, self).__init__()

        self.cfg = cfg
        self.gamma_diag = self.cfg.DIAG
        self.last_step = False
        self.register_flag = False
        self.test_time_adaptation = False
        self.activations = None
        self.mgn_S_gamma = None
        self.phs_S_gamma = None
        self.gamma_init = 0.
        self.gamma_clamp = 1e-16
        self.momentum = 0.01

        self.mgn_gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        self.phs_gamma = nn.parameter.Parameter(torch.Tensor([self.gamma_init]).cuda())
        if self.cfg.MULTIPLICATOR:
            self.mgn_mult = nn.parameter.Parameter(torch.Tensor([0.]).cuda())
            self.phs_mult = nn.parameter.Parameter(torch.Tensor([0.]).cuda())
        self.typePCA = self.cfg.TYPE_PCA
        if self.typePCA == 'incremental':
            self.PCA_mgn = incremental_pca.IncrementalPCA()
            self.PCA_phs = incremental_pca.IncrementalPCA()
        else:
            self.PCA_mgn = pca.PCA()
            self.PCA_phs = pca.PCA()
        self.filter = self.cfg.FILTER

        if 'exp' in self.filter:
            self.fc_filter = self.exp_filter
        elif 'quo' in self.filter:
            self.fc_filter = self.quo_filter
        elif 'cheb' in self.filter:
            self.fc_filter = self.simili_cheb_filter
        else:
            raise NotImplementedError

    def exp_filter(self, gamma, PCA):
        return 1. / (1. + (gamma ** 2 - PCA.explained_variance_).exp())

    def Id_addmult(self, gamma, PCA):
        return 1 + (gamma * PCA.explained_variance_)

    def quo_filter(self, gamma, PCA):
        return PCA.explained_variance_ / (PCA.explained_variance_ + F.relu(gamma))

    def simili_cheb_filter(self, gamma, PCA):
        return 1. / torch.sqrt(1. + gamma / PCA.explained_variance_)

    def forward(self, X):  # Input (N, C, H, W) => (N, C*H*W)
        if self.register_flag:
            with torch.no_grad():
                self.activations = torch.cat(
                    (
                        self.activations,
                        X.cpu()
                    )
                ) if self.activations is not None else X.cpu()
        elif self.test_time_adaptation:
            assert hasattr(self.PCA_mgn, 'V'), "V is not defined. Make sure to self.compute_pca()"
            assert hasattr(self.PCA_mgn, 'Vt'), "Vt is not defined. Make sure to self.compute_pca()"
            assert hasattr(self.PCA_phs, 'V'), "V is not defined. Make sure to self.compute_pca()"
            assert hasattr(self.PCA_phs, 'Vt'), "Vt is not defined. Make sure to self.compute_pca()"

            if self.cfg.MULTIPLICATOR:
                assert self.mgn_mult.requires_grad
                self.mgn_modulator = F.threshold(self.mgn_mult, 1., 1.)
                self.mgn_modulator = self.mgn_modulator.diag()
                assert self.phs_mult.requires_grad
                self.phs_modulator = F.threshold(self.phs_mult, 1., 1.)
                self.phs_modulator = self.phs_modulator.diag()

            # #self.clamp()
            self.mgn_S_gamma = self.fc_filter(self.mgn_gamma, self.PCA_mgn)
            self.phs_S_gamma = self.fc_filter(self.phs_gamma, self.PCA_phs)

            # Reshape and center
            self.mgn_S_gamma = self.mgn_S_gamma.diag()
            self.phs_S_gamma = self.phs_S_gamma.diag()
            X = torch.fft.rfft2(X)
            mgn = torch.abs(X)
            phs = torch.angle(X)
            size = mgn.size()

            mgn = mgn.reshape(size[0], -1)
            phs = phs.reshape(size[0], -1)


            mgn_mean = mgn.mean(0)
            mgn -= mgn_mean
            # mgn += self.PCA_mgn.mean_

            phs_mean = phs.mean(0)
            phs -= phs_mean
            # phs += self.PCA_phs.mean_

            # To PCA basis
            mgn = torch.matmul(mgn, self.PCA_mgn.V)
            phs = torch.matmul(phs, self.PCA_phs.V)

            # Filtering
            if self.last_step: self.mgnbffltr = mgn.t().detach().cpu().numpy()
            mgn = torch.matmul(mgn, self.mgn_S_gamma)
            if self.cfg.MULTIPLICATOR: mgn = torch.matmul(mgn, self.mgn_modulator)
            if self.last_step: self.mgnaffltr = mgn.t().detach().cpu().numpy()
            if self.last_step: self.phsbffltr = phs.t().detach().cpu().numpy()
            phs = torch.matmul(phs, self.phs_S_gamma)
            if self.cfg.MULTIPLICATOR: phs = torch.matmul(phs, self.phs_modulator)
            if self.last_step: self.phsaffltr = phs.t().detach().cpu().numpy()

            # To original basis
            mgn = torch.matmul(mgn, self.PCA_mgn.Vt)
            phs = torch.matmul(phs, self.PCA_phs.Vt)

            # Shift and reshape
            # mgn -= self.PCA_mgn.mean_
            mgn += mgn_mean
            # phs -= self.PCA_phs.mean_
            phs += phs_mean

            mgn = mgn.reshape(size)
            phs = phs.reshape(size)

            X = torch.polar(mgn, phs)
            X = torch.fft.irfft2(X)

        return X

    @torch.no_grad()
    def clamp(self):
        self.gamma.data = torch.clamp(self.gamma.data.abs(), min=self.gamma_clamp)

    def enable_register(self):
        self.register_flag = True  # register_output
        print('Careful: register_flag is now enabled.')

    def compute_pca(self):  # Ici PCA en fin d'epoch, mieux avec IPCA ?

        assert self.activations is not None, "self.activations is empty. Make sure to collect the activations (usually " \
                                             "at last epoch) "
        # if self.cfg['data_type'] == 'freq': pdb.set_trace()

        self.activations = torch.fft.rfft2(self.activations)
        self.activations = self.activations.reshape(self.activations.size(0), -1)
        mgn = torch.abs(self.activations)
        phs = torch.angle(self.activations)

        self.PCA_mgn.fit(A=mgn)
        self.PCA_phs.fit(A=phs)

        self.reset_gamma()
        self.activations = None
        self.covariance = None

    def reset_gamma(self):
        if self.gamma_diag:
            self.mgn_gamma.data = torch.full_like(self.PCA_mgn.singular_values_, self.gamma_clamp).cuda()
            self.phs_gamma.data = torch.full_like(self.PCA_phs.singular_values_, self.gamma_clamp).cuda()
            if self.cfg.MULTIPLICATOR:
                self.mgn_mult.data = torch.full_like(self.PCA_mgn.singular_values_, 1.).cuda()
                self.phs_mult.data = torch.full_like(self.PCA_phs.singular_values_, 1.).cuda()
        else:
            self.mgn_gamma.data = torch.Tensor([self.gamma_clamp]).cuda()
            self.phs_gamma.data = torch.Tensor([self.gamma_clamp]).cuda()
            if self.cfg.MULTIPLICATOR:
                self.mgn_mult.data = torch.Tensor([1.]).cuda()
                self.phs_mult.data = torch.Tensor([1.]).cuda()


def collect_params(model):
    """Collect the affine scale + shift parameters from batch norms.

    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.

    Note: other choices of parameterization are possible!
    """
    params = []
    names = []
    for nm, m in model.named_modules():
        if isinstance(m, GammaLayer):
            for np, p in m.named_parameters():
                params.append(p)
                names.append(f"{nm}.{np}")
    return params, names


def load_model_and_optimizer(model, optimizer, model_state, optimizer_state):
    """Restore the model and optimizer states from copies."""
    model.load_state_dict(model_state, strict=True)
    optimizer.load_state_dict(optimizer_state)


def copy_model_and_optimizer(model, optimizer):
    """Copy the model and optimizer states for resetting after adaptation."""
    model_state = deepcopy(model.state_dict())
    optimizer_state = deepcopy(optimizer.state_dict())
    return model_state, optimizer_state


class ModuledNet(nn.Module):
    def __init__(self, model, cfg):
        super(ModuledNet, self).__init__()
        # self.gamma_layer_2 = GammaLayer()
        if cfg.MODEL.conv1.ENABLED:
            self.conv1 = nn.Sequential(*[model.conv1, GammaLayer(cfg=cfg.MODEL.conv1)])
        else:
            self.conv1 = model.conv1
        if cfg.MODEL.block1.ENABLED:
            self.block1 = nn.Sequential(*[model.block1, GammaLayer(cfg=cfg.MODEL.block1)])
        else:
            self.block1 = model.block1
        if cfg.MODEL.block2.ENABLED:
            self.block2 = nn.Sequential(*[model.block2, GammaLayer(cfg=cfg.MODEL.block2)])
        else:
            self.block2 = model.block2
        if cfg.MODEL.block3.ENABLED:
            self.block3 = nn.Sequential(*[model.block3, GammaLayer(cfg=cfg.MODEL.block3)])
        else:
            self.block3 = model.block3
        self.classifier = ClassifierBlock(
            model.bn1,
            model.relu,
            model.fc,
            model.nChannels
        )
        if cfg.MODEL.CONVLIST.ENABLED:
            L = ['model.' + nm for nm, m in model.named_modules() if isinstance(m, nn.Conv2d)][
                1:]  # model.conv1 special
            for i in range(10):
                remove = '.' + str(i) + '.'
                replace = '[' + str(i) + '].'
                for subind, sub in enumerate(L):
                    if remove in sub: L[subind] = sub[:sub.find(remove)] + replace + sub[
                                                                                     sub.find(remove) + len(remove):]
            for i in cfg.MODEL.CONVLIST.LIST:
                glayer = GammaLayer(cfg=cfg.MODEL.CONVLIST)
                glayer.list_index = i
                exec(f"{L[i]} = nn.Sequential(*[{L[i]}, glayer])")

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.classifier(out)
        return out


class ClassifierBlock(nn.Module):
    def __init__(self, bn, relu, fc, nChannels):
        super(ClassifierBlock, self).__init__()

        self.bn = bn
        self.relu = relu
        self.fc = fc
        self.nChannels = nChannels

    def forward(self, x):
        out = self.relu(self.bn(x))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out)
