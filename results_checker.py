import csv

BN_dict = {'EPS': 1e-5, 'MOM': 0.1}
layer_dict = {'DIAG': True, 'ENABLED': False, 'FILTER': 'exp', 'TYPE_PCA': 'classic', 'VARIANCE_NORM':False,
              'data_type': 'spat', 'C': 1000, 'VOLUME_PRESERVING': False, 'METHOD': None, 'MULTIPLICATOR':False}
optim_dict = {'BETA': 0.9, 'DAMPENING': 0.0, 'LR': 1e-3, 'METHOD': 'Adam', 'MOMENTUM': 0.9, 'NESTEROV': True,
              'STEPS': 1, 'WD': 0.0}
severity_dict = {'5':None, '4':None, '3':None, '2':None, '1':None}
result_dict = {'gaussian_noise':severity_dict, 'shot_noise':severity_dict, 'impulse_noise':severity_dict,
               'defocus_blur':severity_dict, 'glass_blur':severity_dict, 'motion_blur':severity_dict,
               'zoom_blur':severity_dict, 'snow':severity_dict, 'frost':severity_dict, 'fog':severity_dict,
               'brightness':severity_dict, 'contrast':severity_dict, 'elastic_transform':severity_dict,
               'pixelate':severity_dict, 'jpeg_compression':severity_dict}
model_dict = {'BN':layer_dict.update({'RUNNING_STATS': True}),
             'block1':layer_dict, 'block2':layer_dict, 'block3':layer_dict, 'CONVLIST':layer_dict.update({'LIST':[]})}
base_dict = {'ADAPTATION': 'Source', 'ARCH':'Standard', 'EPISODIC': True, 'LOSS':'entropy', 'MODEL':model_dict,
             'OPTIM':optim_dict, 'BN':BN_dict, 'DIV_LOSS':{'enabled':False, 'target_distr':'uniform', 'k':0.9},
             'DATASET':'cifar10', 'RNG_SEED':1, 'BATCH_SIZE': 128, 'RESULTS':result_dict}

# TODO: récupérer l'ancien log_handler

class log_handler():
    def __init__(self):
        super(log_handler, self).__init__()
        self.info_dict = {}

    def read_log(self, dest):
        read_model = False
        read_optim = False
        read_results = False
        read_div_loss = False
        info_dict = base_dict.copy()
        with open(dest, 'r').read() as reader:
            lines = reader.splitlines()
            for i, line in enumerate(lines):
                if 'EPS:' in line:
                    info_dict['BN']['EPS'] = line[line.find('EPS: ')+len('EPS: '):]; continue
                if 'MOM:' in line:
                    info_dict['BN']['MOM'] = line[line.find('MOM: ') + len('MOM: '):]; continue
                if 'LOG_DEST:' in line:
                    ID_run = line[line.find('LOG_DEST: ')+len('LOG_DEST: '):-4]
                    self.info_dict[ID_run] = info_dict; continue
                if 'ADAPTATION:' in line:
                    self.info_dict[ID_run]['ADAPTATION'] = line[line.find('ADAPTATION: ') + len('ADAPTATION: '):]; continue
                if 'DATASET:' in line:
                    self.info_dict[ID_run]['DATASET'] = line[line.find('DATASET: ') + len('DATASET: '):]; continue
                if 'LOSS:' in line:
                    self.info_dict[ID_run]['LOSS'] = line[line.find('LOSS: ') + len('LOSS: '):]; continue
                if 'ARCH:' in line:
                    self.info_dict[ID_run]['ARCH'] = line[line.find('ARCH: ') + len('ARCH: '):]; continue
                if 'EPISODIC:' in line:
                    self.info_dict[ID_run]['EPISODIC'] = line[line.find('EPISODIC: ') + len('EPISODIC: '):]; continue
                if 'MODEL:' in line: read_model = True; continue
                if 'DIV_LOSS:' in line: read_div_loss = True; continue
                if read_div_loss:
                    for key in self.info_dict[ID_run]['DIV_LOSS'][layer].keys():
                        if key in line:
                            self.info_dict[ID_run]['DIV_LOSS'][layer][key] = line[line.find(str(key) + ': ') + len(str(key)) + 2:]
                            if 'target_distr' in key: read_div_loss = False
                            continue
                if 'OPTIM:' in line:
                    read_model = False
                    read_optim = True
                if 'RNG_SEED' in line or 'SAVE_DIR' in line or 'TEST' in line or 'cifar10c.py' in line:
                    read_model = False
                    read_optim = False
                    continue
                if read_model:
                    for layer in ['BN', 'block1', 'block2', 'block3', 'conv1', 'CONVLIST']:
                        for key in self.info_dict[ID_run]['MODEL'][layer].keys():
                            if key in line: self.info_dict[ID_run]['MODEL'][layer][key] = line[line.find(str(key)+': ') + len(str(key))+2:]; continue
                if 'RNG_SEED:' in line:
                    self.info_dict[ID_run]['RNG_SEED'] = line[line.find('RNG_SEED: ') + len('RNG_SEED: '):]; continue
                if read_optim:
                    for key in self.info_dict[ID_run]['OPTIM'].keys():
                        if key in line: self.info_dict[ID_run]['OPTIM'][key] = line[line.find(str(key) + ': ') + len(str(key)) + 2:]; continue
                if 'BATCH_SIZE:' in line:
                    self.info_dict[ID_run]['BATCH_SIZE'] = line[line.find('BATCH_SIZE: ') + len('BATCH_SIZE: '):]; continue
                if 'error ' in line: read_results = True
                if read_results:
                    for key in self.info_dict[ID_run]['RESULTS'].keys():
                        if key in line:
                            self.info_dict[ID_run]['RESULTS'][key][line[line.find(str(key)) + len(str(key)):line.find(str(key)) + len(str(key))+1]] = line[line.find(str(key)) + len(str(key)) + 4:]; continue

    def handle(self):



